import React  from 'react';
import {
    SafeAreaView,
    View,
    FlatList,
    StyleSheet,
    Text,
    Image,
    ImageBackground,
    ScrollView, TouchableOpacity,TextInput
} from "react-native";
const DATA = [
    {
        id:1,
        image: require('../images/Cal.png'),

    },
    {
        id:2,
        image: require('../images/Cal.png'),

    },
    {
        id:3,
        image: require('../images/Cal.png'),

    },
    {
        id:4,
        image: require('../images/Cal.png'),

    },
];

export default class HomeProfile extends React.Component {

    constructor(props) {
        super(props);
        this._onPressButton = this._onPressButton.bind(this);
    }

    _onPressButton(){

        this.props.navigation.navigate('Nutrition');
    }

    render() {
        return (

            <SafeAreaView style={styles.container}>

                <ScrollView>

                    <View style={{flexDirection:'row',marginLeft:'auto'}}>
                        <Image source={require('../images/avatar.png')} style={{width:64,height:70,marginRight:20,marginTop:50}}></Image>
                    </View>
                    <View style={{flexDirection:'row', marginTop:20,marginHorizontal:10,alignItems:'center'}}>
                        <TouchableOpacity  onPress={() => this._onPressBackButton()}>
                            <Image  source={require('../images/day.png')} />
                        </TouchableOpacity>
                        <View style={{flex: 1,marginLeft:15}}>
                            <Text style={{color:'#7265E3',fontSize:15}}>Tues 13 oct</Text>
                        </View>
                    </View>
                    <View style={{marginTop:8,flexDirection:'column',marginLeft:20}}>
                        <Text style={{fontSize:20,color:'#2D3142',fontWeight: 'bold'}}>Hi, Grace</Text>

                    </View>
                    <TouchableOpacity onPress={this._onPressButton} >
                        <View style={{alignItems:'center',marginTop:20}}>
                            <Image source={require('../images/HealthScore.png')}></Image>
                        </View>
                    </TouchableOpacity>


                    <View style={{marginLeft:20,marginRight:20,marginTop:10,flexDirection:'row',justifyContent:'space-between'}}>
                        <Text style={{fontSize:16,color:'#4C5980',fontWeight: 'bold'}}>Metrics </Text>
                        <Image source={require('../images/more.png')}></Image>

                    </View>
                    <View>
                    <FlatList

                        numColumns={2}
                        data={DATA}
                        keyExtriactor={(time, index) => index.toString()}
                        renderItem={({item}) => {
                            return (
                                <View style={{width: 170, height: 352, margin:10}}>
                                    <View style={{width: 170, height: 272, marginLeft: 10,}}>
                                        <ImageBackground source={item.image} style={{
                                            width: '100%',
                                            height: '100%',
                                            borderRadius: 8,
                                            overflow: 'hidden'
                                        }}>

                                        </ImageBackground>
                                    </View>



                                </View>

                            );
                        }}
                    />
                    </View>


                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: '#F2F2F2',
    },
    image: {
        flexGrow:1,
        height:250,
        width:'100%',
        alignItems: 'center',
        justifyContent:'center',
    },

});
