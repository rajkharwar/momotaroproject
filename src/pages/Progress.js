import React  from 'react';
import {
    SafeAreaView,
    View,
    FlatList,
    StyleSheet,
    Text,
    Image,
    ImageBackground,
    ScrollView,
    TouchableOpacity
} from 'react-native';
import Constants from 'expo-constants';

export default class Profile extends React.Component {

    constructor(props) {
        super(props);
        this._onPressButton = this._onPressButton.bind(this);
    }

    _onPressButton(){

        this.props.navigation.navigate('WeightReminder');
    }

    render() {
        return (

            <SafeAreaView style={styles.container}>
                <ScrollView>

                    <View style={{backgroundColor:'#7265E3' ,height:180}}>
                        <View style={{flexDirection:'row', marginTop:20,marginHorizontal:10,alignItems:'center'}}>
                            <TouchableOpacity  onPress={() => this._onPressBackButton()}>
                                <Image  source={require('../images/arrow.png')} />
                            </TouchableOpacity>
                            <View style={{flex: 1,
                                justifyContent: "center",
                                alignItems: "center"}}>
                                <Text style={{color:'#ffffff',fontSize:15}}>Weight</Text>
                            </View>
                        </View>
                        <View style={{flexDirection: 'row',
                            justifyContent:'space-between', marginLeft:25,marginRight:15,marginTop:20}}>
                            <View style={{flexDirection: 'column'}}>
                                <Text style={{color:'#ffffff',fontWeight:'bold'}}>Current</Text>
                                <Text style={{color:'#ffffff',fontWeight:'bold',fontSize:20}}>90 kg</Text>
                            </View>
                            <View style={{flexDirection: 'column'}}>
                                <Image  source={require('../images/weight.png')} />
                            </View>
                            <View style={{flexDirection: 'column'}}>
                                <Text style={{color:'#ffffff',fontWeight:'bold'}}>Target</Text>
                                <Text style={{color:'#ffffff',fontWeight:'bold',fontSize:20}}>88 kg</Text>
                            </View>
                        </View>

                        </View>

                    <View style={{
                        flexDirection: 'row',
                        marginLeft: 15,
                        marginTop: 50,
                        justifyContent: 'space-between'
                    }}>
                        <View style={{flexDirection: 'column', marginLeft: 5}}>
                            <Text style={{fontWeight: 'bold', color: "#000000",fontSize:18}}>My Progress</Text>

                        </View>
                        <View  style={{marginRight: 10,}}>
                            <TouchableOpacity onPress={this._onPressButton} style={{

                                flexDirection: 'row',
                                justifyContent: 'center',
                                backgroundColor: '#E4DFFF',
                                paddingTop: 15,
                                borderRadius: 14,
                                width:120,
                                height:56,
                            }}>
                                <Text style={{color:'#7265E3',fontWeight:'bold'}}>Weekely</Text>
                            </TouchableOpacity>

                        </View>
                    </View>

                    <View style={{alignItems:'center',marginTop:20}}>
                        <Image style={{width:400,height:300}} source={require('../images/Graph.png')}></Image>
                    </View>
                    <View style={{alignItems:'center',marginTop:20}}>
                        <Image style={{width:300,height:130}} source={require('../images/Notes.png')}></Image>
                    </View>
                    <View style={{marginLeft:20,marginRight:20,marginTop:20,flexDirection:'row',justifyContent:'space-between'}}>
                        <Text style={{fontSize:16,color:'#4C5980',fontWeight: 'bold'}}>Journal </Text>
                        <Image source={require('../images/more.png')}></Image>
                    </View>
                    <View style={{marginLeft:20,marginRight:20,marginTop:20,flexDirection:'row',justifyContent:'space-between'}}>
                        <Text style={{fontSize:13,color:'#000000',fontWeight: 'bold'}}>Today </Text>
                        <Text style={{fontSize:13,color:'#000000',fontWeight: 'bold'}}>50.5 </Text>

                    </View>
                    <View style={{marginLeft:20,marginRight:20,marginTop:20,flexDirection:'row',justifyContent:'space-between'}}>
                        <Text style={{fontSize:13,color:'#000000',fontWeight: 'bold'}}>Today </Text>
                        <Text style={{fontSize:13,color:'#000000',fontWeight: 'bold'}}>50.5 </Text>

                    </View>
                    <View style={{marginLeft:20,marginRight:20,marginTop:20,flexDirection:'row',justifyContent:'space-between'}}>
                        <Text style={{fontSize:13,color:'#000000',fontWeight: 'bold'}}>Today </Text>
                        <Text style={{fontSize:13,color:'#000000',fontWeight: 'bold'}}>50.5 </Text>

                    </View>
                    <View style={{alignItems:'center'}}>
                        <TouchableOpacity onPress={this._onPressButton} style={{
                            marginTop: 20,
                            flexDirection: 'row',
                            justifyContent: 'center',
                            backgroundColor: '#7265E3',
                            paddingTop: 15,
                            borderRadius: 14,
                            width:250,
                            height:56
                        }}>
                            <Text style={{color:'#ffffff',fontWeight:'bold'}}>Update Weight</Text>
                        </TouchableOpacity>
                    </View>


                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({

    item: {
        backgroundColor: '#ffffff',
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,

    },
    title: {
        fontSize: 32,
    },
    imageWrapper: {
        height: 200,
        width: 200,
        overflow : "hidden"
    },
    theImage: {
        width: "100%",
        height: "100%",
        resizeMode: "cover",
    },
    container: {
        flex: 1,
        //alignItems: 'stretch',
        //justifyContent: 'center',
        paddingTop: Constants.statusBarHeight,
        //backgroundColor: '#ecf0f1',
    },
    image: {
        //flexGrow:1,
        height:250,
        width:'100%',
        alignItems: 'center',
        justifyContent:'center',
    },
    paragraph: {
        margin: 24,
        fontSize: 48,
        fontWeight: 'bold',
        textAlign: 'center',
        color: 'white',
        backgroundColor: 'transparent',
    },
});
