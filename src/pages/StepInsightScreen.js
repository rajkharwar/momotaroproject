import React  from 'react';
import {
    SafeAreaView,
    View,
    FlatList,
    StyleSheet,
    Text,
    Image,
    ImageBackground,
    ScrollView, TouchableOpacity, TextInput, CheckBox
} from "react-native";


export default class StepInsightScreen extends React.Component {

    constructor(props) {
        super(props);
        this._onPressButton = this._onPressButton.bind(this);
    }

    _onPressButton(){

        this.props.navigation.navigate('WaterScreen');
    }

    render() {
        return (

            <SafeAreaView style={styles.container}>

                <ScrollView>

                    <View style={{
                        flexDirection: 'row',
                        marginLeft: 20,
                        marginTop: 50,}}>
                            <TouchableOpacity  onPress={() => this._onPressBackButton()}>
                                <Image  source={require('../images/down.png')} />
                            </TouchableOpacity>
                    </View>
                    <View style={{marginTop:20, marginLeft: 20}}>
                        <Text style={{color:'#000000',fontSize:25,fontWeight:'bold'}}>Insight</Text>
                    </View>

                    <TouchableOpacity onPress={this._onPressButton} >
                        <View style={{alignItems:'center',marginTop:20}}>
                            <Image source={require('../images/insight1.png')}></Image>
                        </View>
                    </TouchableOpacity>
                    <View style={{alignItems:'center',marginTop:20}}>
                        <Image source={require('../images/insight2.png')}></Image>
                    </View>

                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: '#F2F2F2',
    },
    image: {
        flexGrow:1,
        height:250,
        width:'100%',
        alignItems: 'center',
        justifyContent:'center',
    },
    nutList:{
        marginLeft:20,
        marginRight:20,
        marginTop:25,
    }

});
