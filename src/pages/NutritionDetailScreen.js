import React  from 'react';
import {
    SafeAreaView,
    View,
    FlatList,
    StyleSheet,
    Text,
    Image,
    ImageBackground,
    ScrollView,
    TouchableOpacity
} from 'react-native';
import Constants from 'expo-constants';
import { Picker } from 'react-native-picker-dropdown'
const DATA = [
    {
        id:1,
        image: require('../images/calories.png'),
        title: 'Salad with wheat and white egg',
        desc: '200 cals',

    },
    {
        id:2,
        image: require('../images/calories.png'),
        title: 'Salad with wheat and white egg',
        desc: '200 cals',

    },
    {
        id:3,
        image: require('../images/calories.png'),
        title: 'Salad with wheat and white egg',
        desc: '200 cals',

    },
    {
        id:4,
        image: require('../images/calories.png'),
        title: 'Salad with wheat and white egg',
        desc: '200 cals',

    },
];


export default class NutritionDetailScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = { language: 'js'  }
        this.onValueChange = this.handleValueChange.bind(this)
        this._onPressButton = this._onPressButton.bind(this);
    }
    handleValueChange(language) {
        this.setState({ language })
    }


    _onPressButton(){

        this.props.navigation.navigate('RecipeScreen');
    }

    handleChange = (id) => {
        let temp = this.state.products.map((product) => {
            if (id === product.id) {
                return { ...product, isChecked: !product.isChecked };
            }
            return product;
        });
        this.setState({
            products: temp,
        });
    };


    renderFlatList = (renderData) => {
        return (
            <FlatList
                data={renderData}
                renderItem={({ item }) => (

                    <View style={styles.listItem}>
                        <Image source={item.image}  style={{width:60, height:60,borderRadius:30}} />
                        <View style={{alignItems:"center",flex:1}}>
                            <Text style={{fontWeight:"bold"}}>{item.title}</Text>
                            <Text>{item.desc}</Text>
                        </View>
                    </View>
                )}
            />
        );
    };


    render() {
        return (

            <SafeAreaView style={styles.container}>
                <ScrollView>

                    <View style={{backgroundColor:'#7265E3' ,height:380}}>
                        <View style={{flexDirection:'row', marginTop:20,marginHorizontal:10,alignItems:'center'}}>
                            <TouchableOpacity  onPress={() => this._onPressBackButton()}>
                                <Image  source={require('../images/arrow.png')} />
                            </TouchableOpacity>
                            <View style={{flex: 1,
                                justifyContent: "center",
                                alignItems: "auto"}}>
                                <Image  source={require('../images/heartSmall.png')} />
                            </View>
                        </View>

                    </View>


                    <View style={{backgroundColor:'#FFFFFF',paddingBottom:20}}>
                        <View style={{
                            flexDirection: 'row',
                            marginLeft: 15,
                            marginTop: 20,
                            justifyContent: 'space-between'
                        }}>
                            <View style={{flexDirection: 'column', marginLeft: 5}}>
                                <Text style={{color: "#7265E3",fontSize:15}}>Nutrition</Text>
                                <View style={{flexDirection:'row', marginTop:10,alignItems:'center'}}>

                                    <Text style={{color:'#2D3142',fontSize:20,fontWeight:'bold'}}>Salad with wheat and white egg breakfast</Text>
                                </View>
                                <View style={{flexDirection:'row', marginTop:20,alignItems:'center'}}>

                                    <Image  source={require('../images/Shape.png')} style={{
                                        height: 30,
                                        width: 25}} />

                                    <View style={{marginLeft:20}}>
                                        <Text style={{color:'#7265E3',fontSize:20}}>345 kcal</Text>
                                    </View>
                                </View>
                                <View>
                                    <Picker
                                        selectedValue={this.state.language}
                                        onValueChange={this.onValueChange}
                                        prompt="Choose your favorite language"
                                        style={styles.picker}
                                        textStyle={styles.pickerText}
                                        cancel
                                    >
                                        <Picker.Item label="JavaScript" value="js" />
                                        <Picker.Item label="Ruby" value="ruby" />
                                        <Picker.Item label="Python" value="python" />
                                        <Picker.Item label="Elm" value="elm" />
                                    </Picker>
                                </View>
                                <View style={{flexDirection:'row', marginTop:20,marginHorizontal:10,marginLeft:15}}>

                                        <Image  source={require('../images/Bookmark.png')} />
                                    <View style={{flex: 1,justifyContent:'center',alignItems:'center'}}>
                                        <TouchableOpacity onPress={this._onPressButton} style={{
                                            flexDirection: 'row',
                                            justifyContent: 'center',
                                            backgroundColor: '#7265E3',
                                            paddingTop: 15,
                                            borderRadius: 14,
                                            width:230,
                                            height:56
                                        }}>
                                            <Text style={{color:'#FFFFFF',fontWeight:'bold'}}>Add To Journal</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                <View style={{flexDirection:'row', marginTop:20,alignItems:'center'}}>

                                    <Text style={{color:'#2D3142',fontSize:15,fontWeight:'bold'}}>Nutrition Fact</Text>
                                </View>
                                <View style={{
                                    flexDirection: 'row',
                                    marginLeft: 15,
                                    marginTop:20,
                                    //justifyContent: 'space-between'
                                }}>
                                <View style={{flexDirection: 'column', marginLeft: 10}}>
                                        <Image  source={require('../images/Graph1.png')} />
                                        <Text style={{textAlign: 'center'}}>Carbs</Text>
                                </View>
                                    <View style={{flexDirection: 'column', marginLeft: 10}}>
                                        <Image  source={require('../images/Graph1.png')} />
                                        <Text style={{textAlign: 'center'}}>Proteins</Text>
                                    </View>
                                    <View style={{flexDirection: 'column', marginLeft: 10}}>
                                        <Image  source={require('../images/Graph1.png')} />
                                        <Text style={{textAlign: 'center'}}>Fats</Text>
                                    </View>


                                </View>
                                <View style={{
                                    flexDirection: 'row',
                                    marginLeft: 15,
                                    marginTop:30,
                                    //justifyContent: 'space-between'
                                }}>
                                    <View style={{flexDirection: 'column', marginLeft: 10}}>
                                        <Image  source={require('../images/Rectangle.png')} />

                                    </View>
                                    <View style={{flexDirection: 'column', marginLeft: 10}}>
                                        <Text style={{fontWeight:'bold'}}>Proteins</Text>
                                    </View>
                                    <View style={{flexDirection: 'column', marginLeft: 'auto',marginRight:20}}>
                                        <Text style={{fontWeight:'bold'}}>4gm</Text>
                                    </View>


                                </View>

                                <View style={{
                                    flexDirection: 'row',
                                    marginLeft: 15,
                                    marginTop:30,
                                    //justifyContent: 'space-between'
                                }}>
                                    <View style={{flexDirection: 'column', marginLeft: 10}}>
                                        <Image  source={require('../images/Rectangle.png')} />

                                    </View>
                                    <View style={{flexDirection: 'column', marginLeft: 10}}>
                                        <Text style={{fontWeight:'bold'}}>Carbs</Text>
                                        <Text style={{marginTop:10}}>Fibres</Text>
                                        <Text style={{marginTop:10}}>Sugar</Text>
                                    </View>

                                    <View style={{flexDirection: 'column', marginLeft: 'auto',marginRight:20}}>
                                        <Text style={{fontWeight:'bold'}}>4gm</Text>
                                        <Text style={{marginTop:10}}>40g</Text>
                                        <Text style={{marginTop:10}}>60g</Text>
                                    </View>
                                </View>
                                <View style={{
                                    flexDirection: 'row',
                                    marginLeft: 15,
                                    marginTop:30,
                                    //justifyContent: 'space-between'
                                }}>
                                    <View style={{flexDirection: 'column', marginLeft: 10}}>
                                        <Image  source={require('../images/Rectangle.png')} />

                                    </View>
                                    <View style={{flexDirection: 'column', marginLeft: 10}}>
                                        <Text style={{fontWeight:'bold'}}>Fats</Text>
                                        <Text style={{marginTop:10}}>Saturated Fats</Text>
                                        <Text style={{marginTop:10}}>Unsaturated Fats</Text>
                                    </View>
                                    <View style={{flexDirection: 'column', marginLeft: 'auto',marginRight:20}}>
                                        <Text style={{fontWeight:'bold'}}>4gm</Text>
                                        <Text style={{marginTop:10}}>40g</Text>
                                        <Text style={{marginTop:10}}>60g</Text>
                                    </View>
                                </View>
                                <TouchableOpacity onPress={this._onPressButtons} >
                                    <View style={{alignItems:'center',marginTop:20}}>
                                        <Image source={require('../images/Comment.png')}></Image>
                                    </View>
                                </TouchableOpacity>
                            </View>



                        </View>


                    </View>

                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({

    item: {
        backgroundColor: '#ffffff',
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,

    },
    title: {
        fontSize: 32,
    },

    container: {
        flex: 1,
        //alignItems: 'stretch',
        //justifyContent: 'center',
        paddingTop: Constants.statusBarHeight,
        //backgroundColor: '#ecf0f1',
    },
    image: {
        //flexGrow:1,
        height:250,
        width:'100%',
        alignItems: 'center',
        justifyContent:'center',
    },
    listItem:{
        margin:10,
        //  padding:10,
        backgroundColor:"#FFF",
        width:"80%",
        flex:1,
        alignSelf:"center",
        flexDirection:"row",
        borderRadius:5
    },
    picker: {
        alignSelf: 'stretch',
        backgroundColor: '#F4F6FA',
        paddingHorizontal: 10,
        paddingVertical: 10,
        margin: 20,
        borderRadius: 10,
    },
    pickerText: {
        color: '#000000',
    }
});
