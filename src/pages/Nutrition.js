import React  from 'react';
import {
    SafeAreaView,
    View,
    FlatList,
    StyleSheet,
    Text,
    Image,
    ImageBackground,
    ScrollView, TouchableOpacity, TextInput, CheckBox
} from "react-native";
const DATA = [
    {
        id:1,
        image: require('../images/Rectangle.png'),
        text: 'carbs',
        weight:'100g',
        percentage:'42%',

    },
    {
        id:2,
        image: require('../images/Rectangle.png'),
        text: 'carbs',
        weight:'100g',
        percentage:'42%',

    },
    {
        id:3,
        image: require('../images/Rectangle.png'),
        text: 'carbs',
        weight:'100g',
        percentage:'42%',

    },
    {
        id:4,
        image: require('../images/Rectangle.png'),
        text: 'carbs',
        weight:'100g',
        percentage:'42%',

    },
];

export default class Nutrition extends React.Component {

    constructor(props) {
        super(props);
        this.state = {products: DATA,};
        this._onPressButton = this._onPressButton.bind(this);
    }

    _onPressButton(){

        this.props.navigation.navigate('JournalScreen');
    }
    handleChange = (id) => {
        let temp = this.state.products.map((product) => {
            if (id === product.id) {
                return { ...product, isChecked: !product.isChecked };
            }
            return product;
        });
        this.setState({
            products: temp,
        });
    };

    renderFlatList = (renderData) => {
        return (
            <FlatList
                data={renderData}
                renderItem={({ item }) => (
                    <View style={{ margin: 2 }}>
                        <View style={styles.nutList}>
                            <View
                                style={{
                                    flexDirection: 'row',
                                    flex: 1,
                                    justifyContent: 'space-between',
                                }}>
                                <Image source={item.image} style={{
                                    //width: '100%',
                                   // height: '100%',
                                    //borderRadius: 8,
                                    overflow: 'hidden'
                                }}>
                                </Image>
                                <Text>{item.text}</Text>
                                <Text>{item.weight}</Text>
                                <Text>{item.percentage}</Text>

                            </View>
                        </View>
                    </View>
                )}
            />
        );
    };
    render() {
        return (

            <SafeAreaView style={styles.container}>

                <ScrollView>

                    <View style={{
                        flexDirection: 'row',
                        marginLeft: 15,
                        marginTop: 50,
                        justifyContent: 'space-between'
                    }}>
                        <View style={{flexDirection: 'column', marginLeft: 5}}>
                            <TouchableOpacity  onPress={() => this._onPressBackButton()}>
                                <Image  source={require('../images/down.png')} />
                            </TouchableOpacity>

                        </View>
                        <View  style={{marginRight: 10,}}>
                            <TouchableOpacity onPress={this._onPressButton} style={{

                                flexDirection: 'row',
                                justifyContent: 'center',
                                backgroundColor: '#E4DFFF',
                                paddingTop: 15,
                                borderRadius: 14,
                                width:120,
                                height:56,
                            }}>
                                <Text style={{color:'#7265E3',fontWeight:'bold'}}>JOURNAL</Text>
                            </TouchableOpacity>

                        </View>
                    </View>
                    <View style={{alignItems: 'center',marginTop:20}}>
                        <Text style={{color:'#7265E3',fontSize:15}}>DAILY INTAKE</Text>
                    </View>
                    <View style={{alignItems: 'center',justifyContent:'center',alignRight:'auto'}}>
                        <Text style={{color: '#2D3142',fontSize:20,fontWeight: 'bold'}}>
                            Today you have consumed{' '}
                            <Text style={{color: '#7265E3',fontSize:20,fontWeight: 'bold'}}>
                                   500 cal
                            </Text>
                        </Text>
                    </View>
                    <TouchableOpacity onPress={this._onPressButton} >
                        <View style={{alignItems:'center',marginTop:20}}>
                            <Image source={require('../images/GraphNut.png')}></Image>
                        </View>
                    </TouchableOpacity>
                    <View>
                        <View style={{ flex: 1 }}>
                            {this.renderFlatList(this.state.products)}
                        </View>
                    </View>
                    <View style={{alignItems:'center'}}>
                        <TouchableOpacity onPress={this._onPressButton} style={{
                            marginTop: 20,
                            flexDirection: 'row',
                            justifyContent: 'center',
                            backgroundColor: '#7265E3',
                            paddingTop: 15,
                            borderRadius: 14,
                            width:250,
                            height:56
                        }}>
                            <Text style={{color:'#ffffff',fontWeight:'bold'}}>Add Meals</Text>
                        </TouchableOpacity>
                    </View>

                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: '#F2F2F2',
    },
    image: {
        flexGrow:1,
        height:250,
        width:'100%',
        alignItems: 'center',
        justifyContent:'center',
    },
    nutList:{
        marginLeft:20,
        marginRight:20,
        marginTop:25,
    }

});
