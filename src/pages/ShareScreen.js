import React  from 'react';
import {
    SafeAreaView,
    View,
    FlatList,
    StyleSheet,
    Text,
    Image,
    ImageBackground,
    ScrollView, TouchableOpacity, TextInput, CheckBox
} from "react-native";
const DATA = [
    {
        id:1,
        image: require('../images/Bg1.png'),


    },
    {
        id:2,
        image: require('../images/Bg1.png'),

    },
    {
        id:3,
        image: require('../images/Bg1.png'),


    },
    {
        id:4,
        image: require('../images/Bg1.png'),


    },
];

export default class ShareScreen extends React.Component {

    constructor(props) {
        super(props);
        this._onPressButton = this._onPressButton.bind(this);
    }

    _onPressButton(){

        this.props.navigation.navigate('CommunityScreen');
    }

    render() {
        return (

            <SafeAreaView style={styles.container}>

                <ScrollView>

                    <View style={{
                        flexDirection: 'row',
                        marginLeft: 15,
                        marginTop: 50,
                        justifyContent: 'space-between'
                    }}>
                        <View style={{flexDirection: 'column', marginLeft: 5}}>
                            <TouchableOpacity  onPress={() => this._onPressBackButton()}>
                                <Image  source={require('../images/down.png')} />
                            </TouchableOpacity>
                        </View>
                        <View style={{flexDirection: 'column', marginLeft: 5}}>
                            <Text style={{color:'#2D3142',fontWeight:'bold'}}>Share</Text>

                        </View>
                        <View  style={{marginRight: 10,}}>
                            <TouchableOpacity onPress={this._onPressButton} style={{
                                width:70,
                                height:46,
                            }}>

                                    <Image  source={require('../images/download.png')} />

                            </TouchableOpacity>

                        </View>
                    </View>


                        <View style={{alignItems:'center',marginTop:20}}>
                            <Image source={require('../images/shareCard.png')}></Image>
                        </View>

                    <View style={{backgroundColor:'#FFFFFF',marginTop:20,borderTopRightRadius:40,borderTopLeftRadius:40,paddingBottom:20,height: 400}}>
                        <View style={{
                            flexDirection: 'row',
                            marginLeft: 15,
                            marginTop: 20,
                            justifyContent: 'space-between'
                        }}>
                            <View style={{flexDirection: 'column', marginLeft: 5}}>
                                <Text style={{color: "#7265E3",fontSize:15}}>CHANGE CARD</Text>
                                <View style={{flexDirection:'row', marginTop:20,alignItems:'center'}}>

                                    <TouchableOpacity onPress={this._onPressButtons} style={{

                                        flexDirection: 'row',
                                        justifyContent: 'center',
                                        backgroundColor: '#E4DFFF',
                                        paddingTop: 10,
                                        borderRadius: 14,
                                        width:50,
                                        height:50,
                                    }}>
                                        <Image  source={require('../images/camera.png')} />
                                    </TouchableOpacity>

                                </View>
                                <View>
                                    <FlatList
                                        horizontal={true}
                                        showsHorizontalScrollIndicator={false}
                                        data={DATA}
                                        keyExtriactor={(time, index) => index.toString()}
                                        renderItem={({item}) => {
                                            return (
                                                <View style={{width: 100,height: 100,marginLeft: 20,marginTop: 20}}>
                                                            <Image source={item.image}>
                                                            </Image>
                                                </View>


                                            );
                                        }}
                                    />

                                </View>
                                <View style={{alignItems:'center'}}>
                                    <TouchableOpacity onPress={this._onPressButton} style={{
                                        marginTop: 20,
                                        flexDirection: 'row',
                                        justifyContent: 'center',
                                        backgroundColor: '#7265E3',
                                       // paddingTop: 15,
                                        borderRadius: 14,
                                        width:250,
                                        height:56
                                    }}>
                                        <View style={{flexDirection:'row',alignItems:'center'}}>

                                            <Image  source={require('../images/share.png')} style={{
                                                height: 10,
                                                width: 15}} />

                                            <View style={{marginLeft:20}}>
                                                <Text style={{color:'#FFFFFF',fontSize:18}}>Share</Text>
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                </View>

                            </View>
                        </View>
                    </View>


                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: '#F2F2F2',
    },
    image: {
        flexGrow:1,
        height:250,
        width:'100%',
        alignItems: 'center',
        justifyContent:'center',
    },
    nutList:{
        marginLeft:20,
        marginRight:20,
        marginTop:25,
    }

});
