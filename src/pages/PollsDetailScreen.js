import React  from 'react';
import {
    SafeAreaView,
    View,
    FlatList,
    StyleSheet,
    Text,
    Image,
    ImageBackground,
    ScrollView,
    TouchableOpacity,
    TextInput
} from 'react-native';
import Constants from 'expo-constants';
import AppIntroSlider from "react-native-app-intro-slider";
//import Search from "react-native-search-box";
//import SearchList from "react-native-search-list";
//import {TextInput} from "react-native-web";



const rowHeight = 45;


export default class PollsDetailScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = {CommentName:''};
        this._onPressButton = this._onPressButton.bind(this);
    }

    _onPressButton(){

        this.props.navigation.navigate('CreatePostScreen');
    }



    render() {
        return (

            <SafeAreaView style={styles.container}>
                <ScrollView>

                    <View style={{backgroundColor:'#FFDE86' ,height:180}}>
                        <View style={{
                            flexDirection: 'row',
                            marginLeft: 20,
                            marginTop: 20,}}>
                            <TouchableOpacity  onPress={() => this._onPressBackButton()}>
                                <Image  source={require('../images/down.png')} />
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={{flexGrow: 1,alignItems: 'center'}}>
                        <Image
                            source={require('../images/banana.png')}
                            style={{
                                position: 'absolute',
                                top: -70,
                                height: 120,
                                width: 150}}
                        />
                    </View>
                    <View style={{alignItems: 'center',marginTop:50}}>
                        <Text style={{color:'#7265E3',fontSize:15}}>HEALTH POLLS</Text>
                        <View style={{marginLeft:30,marginTop:20}}>
                        <Text style={{color: '#2D3142',fontSize:18,fontWeight: 'bold'}}>
                            Do Bananas Cause Weight Gain or {''}

                            <Text style={{color: '#2D3142',fontSize:18,fontWeight: 'bold'}}>
                                Help With Weight Loss?
                            </Text>
                        </Text>
                        </View>
                    </View>
                    <View style={{alignItems:'center',marginTop:20}}>
                        <Image source={require('../images/answer1.png')}></Image>
                    </View>
                    <View style={{alignItems:'center',marginTop:20}}>
                        <Image source={require('../images/answer2.png')}></Image>
                    </View>

                    <View style={{
                        flexDirection: 'row',
                        marginLeft: 15,
                        marginTop: 50,
                       // justifyContent: 'space-between'
                    }}>
                        <View style={{flexDirection: 'column', marginLeft: 15}}>

                                <Image  source={require('../images/heart1.png')} />
                        </View>
                        <View style={{flexDirection: 'column', marginLeft: 15}}>
                            <Text style={{color:'#2D3142',fontWeight:'bold'}}>45</Text>

                        </View>
                        <View style={{flexDirection: 'column', marginLeft: 15}}>

                            <Image  source={require('../images/comment1.png')} />
                        </View>
                        <View style={{flexDirection: 'column', marginLeft: 15}}>
                            <Text style={{color:'#2D3142',fontWeight:'bold'}}>4</Text>

                        </View>
                        <View style={{ flexDirection: 'row',marginLeft: 'auto'}}>
                        <View style={{flexDirection: 'column'}}>
                            <Image  source={require('../images/share1.png')} />
                        </View>
                        <View style={{flexDirection: 'column',marginLeft:10,marginRight:20}}>
                            <Text style={{color:'#7265E3',fontWeight:'bold'}}>Share</Text>
                        </View>
                        </View>
                    </View>
                    <View style={{
                        flexDirection: 'row',
                        marginLeft: 15,
                        marginTop: 50,
                        // justifyContent: 'space-between'
                    }}>
                        <TouchableOpacity onPress={this._onPressButton}>
                        <View style={{flexDirection: 'column', marginLeft: 15}}>

                            <Image  source={require('../images/avatar.png')} />
                        </View>
                        </TouchableOpacity>
                        <View style={{flexDirection: 'column', marginLeft: 15}}>
                            <Text style={{color:'#2D3142',fontWeight:'bold'}}>Marie Smith</Text>
                            <Text style={{color:'#2D3142'}}>1 min ago</Text>
                            <View>
                            <Text style={{color:'#4C5980'}}>Bananas are healthy and nutritious,</Text>
                                <Text style={{color:'#4C5980'}}> there is no doubt about that. They</Text>
                                <Text style={{color:'#4C5980'}}>are also high in fiber, but low in calories. </Text>
                            </View>
                        </View>


                    </View>
                    <View style={{alignItems:'center'}}>

                        <TextInput style={styles.inputtext} onChangeText={(CommentName) => this.setState({CommentName})}
                                   value={this.state.CommentName}   placeholder = "Write Comment " underlineColorAndroid='transparent' placeholderTextColor='#969490'/>
                    </View>






                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({

    item: {
        backgroundColor: '#ffffff',
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,

    },
    title: {
        fontSize: 32,
    },
    imageWrapper: {
        height: 200,
        width: 200,
        overflow : "hidden"
    },
    theImage: {
        width: "100%",
        height: "100%",
        resizeMode: "cover",
    },
    container: {
        flex: 1,
        //alignItems: 'stretch',
        //justifyContent: 'center',
        paddingTop: Constants.statusBarHeight,
        //backgroundColor: '#ecf0f1',
    },
    inputtext:{
        marginTop:20,
        height: 60,
        width:300,
        backgroundColor: '#F4F6FA',
        borderRadius:10,
        fontSize: 18,
        paddingHorizontal : 16,

    },

});
