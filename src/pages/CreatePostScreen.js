import React, { useState, useEffect } from 'react';
import {
    SafeAreaView,
    View,
    FlatList,
    StyleSheet,
    Text,
    Image,
    CheckBox,
    ImageBackground,
    ScrollView,
    TouchableOpacity, TextInput
} from 'react-native';
import Constants from 'expo-constants';

const DATA = [
    {
        id:1,
        image: require('../images/PhotoCamera.png'),
        name: 'Photo',

    },
    {
        id:2,
        image: require('../images/PhotoCamera.png'),
        name: 'Health',

    },
    {
        id:3,
        image: require('../images/PhotoCamera.png'),
        name: 'Step',

    },
    {
        id:4,
        image: require('../images/PhotoCamera.png'),
        name: 'Water',

    },
    {
        id:5,
        image: require('../images/PhotoCamera.png'),
        name: 'Water',

    },
    {
        id:6,
        image: require('../images/PhotoCamera.png'),
        name: 'Water',

    },
    {
        id:7,
        image: require('../images/PhotoCamera.png'),
        name: 'Water',

    },
    {
        id:8,
        image: require('../images/PhotoCamera.png'),
        name: 'Water',

    },
    {
        id:9,
        image: require('../images/PhotoCamera.png'),
        name: 'Water',

    },
];


export default class CreatePostScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = {CommentName:''};
        this._onPressButton = this._onPressButton.bind(this);
    }
    _onPressButton(){

        this.props.navigation.navigate('InviteScreen');
    }

    render() {
        return (

            <SafeAreaView style={styles.container}>
                <ScrollView>

                    <View style={{backgroundColor:'#000000' ,height:100}}>

                    </View>
                    <View style={{
                        flexDirection: 'row',
                        marginLeft: 15,
                        marginTop: 20,
                        justifyContent: 'space-between'
                    }}>
                        <View style={{flexDirection: 'column', marginLeft: 5}}>
                            <TouchableOpacity  onPress={() => this._onPressBackButton()}>
                                <Image  source={require('../images/arrowdown.png')} />
                            </TouchableOpacity>

                        </View>
                        <View  style={{marginRight: 10,}}>
                            <TouchableOpacity onPress={this._onPressButton} style={{

                                flexDirection: 'row',
                                justifyContent: 'center',
                                backgroundColor: '#E4DFFF',
                                paddingTop: 15,
                                borderRadius: 14,
                                width:120,
                                height:46,
                            }}>
                                <Text style={{color:'#7265E3',fontWeight:'bold'}}>Post</Text>
                            </TouchableOpacity>

                        </View>
                    </View>
                    <View style={{marginTop:20, marginLeft: 20}}>
                        <Text style={{color:'#2D3142',fontSize:20,fontWeight:'bold'}}>Create Post</Text>
                    </View>

                    <View style={{
                        flexDirection: 'row',
                        marginLeft: 15,
                        marginTop: 50,
                        // justifyContent: 'space-between'
                    }}>
                        <TouchableOpacity onPress={this._onPressButton}>
                            <View style={{flexDirection: 'column', marginLeft: 15}}>

                                <Image  source={require('../images/avatar.png')} />
                            </View>
                        </TouchableOpacity>
                        <View style={{flexDirection: 'column', marginLeft: 15}}>
                            <TextInput style={styles.inputtext} onChangeText={(CommentName) => this.setState({CommentName})}
                                       value={this.state.CommentName}   placeholder = "What's on your mind? " underlineColorAndroid='transparent' placeholderTextColor='#969490'/>
                        </View>
                    </View>
                    <View style={{marginTop:40, marginLeft: 20}}>
                        <Text style={{color:'#2D3142',fontSize:15,fontWeight:'bold'}}>Add attachment</Text>
                    </View>
                    <View>
                        <FlatList

                            numColumns={3}
                            data={DATA}
                            keyExtriactor={(time, index) => index.toString()}
                            renderItem={({item}) => {
                                return (
                                    <View style={{width: 70, height: 70, margin:25}}>
                                        <View style={{width: 70, height: 72, marginLeft: 10,}}>
                                            <ImageBackground source={item.image} style={{
                                                width: 70,
                                                height: 70,
                                                borderRadius: 8,
                                                overflow: 'hidden'
                                            }}>

                                            </ImageBackground>
                                            <View style={{alignItems:'center'}}>
                                            <Text>{item.name}</Text>
                                            </View>

                                        </View>



                                    </View>

                                );
                            }}
                        />
                    </View>
                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        //alignItems: 'stretch',
        //justifyContent: 'center',
        paddingTop: Constants.statusBarHeight,
        //backgroundColor: '#ecf0f1',
    },
    inputtext:{
        //marginTop:20,
        height: 60,
        width:300,
        backgroundColor: '#F4F6FA',
        borderRadius:10,
        fontSize: 18,
        paddingHorizontal : 16,

    },
});
