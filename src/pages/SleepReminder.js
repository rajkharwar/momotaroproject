import React, { useState, useEffect } from 'react';
import {
    SafeAreaView,
    View,
    FlatList,
    StyleSheet,
    Text,
    Image,
    ImageBackground,
    ScrollView,
    TouchableOpacity
} from 'react-native';
import Constants from 'expo-constants';


export default class SleepReminder extends React.Component {

    constructor(props) {
        super(props);

        this._onPressButton = this._onPressButton.bind(this);
    }


    _onPressButton(){

        this.props.navigation.navigate('HomeProfile');
    }

    render() {

        return (

            <SafeAreaView style={styles.container}>
                <ScrollView>

                    <View style={{backgroundColor:'#000000' ,height:100}}>

                    </View>
                    <View>
                        <Image source={require('../images/arrowdown.png')} style={{width:24,height:24,marginLeft:20,marginTop:20}}></Image>
                    </View>

                    <View style={{alignItems:'center',marginTop:20}}>
                        <Image style={{width:300,height:200}} source={require('../images/sleepReminder.png')}></Image>
                    </View>

                    <View style={{marginLeft:20,marginRight:20,marginTop:20,flexDirection:'row',justifyContent:'center'}}>
                        <Text style={{fontSize:16,color:'#4C5980',fontWeight: 'bold'}}>What time would you like to sleep? </Text>

                    </View>
                    <View style={{marginLeft:20,marginRight:20,marginTop:20,flexDirection:'row',justifyContent:'center',alignItems: 'center'}}>
                        <Text style={{fontSize:13,color:'#4C5980'}}>Sets a reminder to alert you at what point you should go to sleep. </Text>

                    </View>


                    <View style={{alignItems:'center'}}>
                        <TouchableOpacity onPress={this._onPressButton} style={{
                            marginTop: 20,
                            flexDirection: 'row',
                            justifyContent: 'center',
                            backgroundColor: '#7265E3',
                            paddingTop: 15,
                            borderRadius: 14,
                            width:250,
                            height:56
                        }}>
                            <Text style={{color:'#ffffff',fontWeight:'bold'}}>Set Reminder</Text>
                        </TouchableOpacity>
                    </View>


                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        //alignItems: 'stretch',
        //justifyContent: 'center',
        paddingTop: Constants.statusBarHeight,
        //backgroundColor: '#ecf0f1',
    },
    image: {
        //flexGrow:1,
        height:250,
        width:'100%',
        alignItems: 'center',
        justifyContent:'center',
    },

});
