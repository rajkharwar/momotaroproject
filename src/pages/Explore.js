import React  from 'react';
import {
  SafeAreaView,
  View,
  FlatList,
  StyleSheet,
  Text,
  Image,
  ImageBackground,
  ScrollView, TouchableOpacity,TextInput
} from "react-native";


export default class Explore extends React.Component {

  constructor(props) {
    super(props);
    this.state = {password : "", LoginId : "",ClientId:'',FirstName:''};
    this._onPressButton = this._onPressButton.bind(this);
  }

  _onPressButton(){

    this.props.navigation.navigate('Challenges');
  }

  render() {
    return (

      <SafeAreaView style={styles.container}>

        <ScrollView>

          <View>
            <Image source={require('../images/down.png')} style={{width:24,height:24,marginLeft:20,marginTop:40}}></Image>
          </View>
          <View>

            <Text style={{color:'#2D3142',marginLeft:20,marginTop:10,fontSize:32,fontWeight:'bold'}}>Explore</Text>
          </View>

          <View style={{alignItems:'center',marginTop:20}}>
            <Image source={require('../images/Search1.png')}></Image>
          </View>
          <TouchableOpacity onPress={this._onPressButton} >
          <View style={{alignItems:'center',marginTop:20}}>
            <Image source={require('../images/Article1.png')}></Image>
          </View>
          </TouchableOpacity>

          <View style={{alignItems:'center',marginTop:20}}>
            <Image source={require('../images/Topic.png')}></Image>
          </View>
          <View style={{alignItems:'center',marginTop:20}}>
            <Image source={require('../images/GetInspired.png')}></Image>
          </View>

          <View style={{alignItems:'center',marginTop:20}}>
            <Image source={require('../images/Collection.png')}></Image>
          </View>

        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({

  item: {
    backgroundColor: '#F2F2F2',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },
  imageWrapper: {
    height: 200,
    width: 200,
    overflow : "hidden"
  },
  theImage: {
    width: "100%",
    height: "100%",
    resizeMode: "cover",
  },
  container: {
    flex: 1,
    backgroundColor: '#F2F2F2',
    //alignItems: 'stretch',
    //justifyContent: 'center',
    //backgroundColor: '#ecf0f1',
  },
  image: {
    flexGrow:1,
    height:250,
    width:'100%',
    alignItems: 'center',
    justifyContent:'center',
  },
  paragraph: {
    margin: 24,
    fontSize: 48,
    fontWeight: 'bold',
    textAlign: 'center',
    color: 'white',
    backgroundColor: 'transparent',
  },
});
