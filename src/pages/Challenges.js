import React  from 'react';
import {
    SafeAreaView,
    View,
    FlatList,
    StyleSheet,
    Text,
    Image,
    ImageBackground,
    ScrollView, TouchableOpacity,TextInput
} from "react-native";


export default class Challenges extends React.Component {

    constructor(props) {
        super(props);
        this._onPressButton = this._onPressButton.bind(this);
    }

    _onPressButton(){

        this.props.navigation.navigate('Profile');
    }

    render() {
        return (

            <SafeAreaView style={styles.container}>

                <ScrollView>

                    <View>
                        <Image source={require('../images/down.png')} style={{width:24,height:24,marginLeft:20,marginTop:40}}></Image>
                    </View>

                    <View style={{marginTop:10,flexDirection:'column',marginLeft:20}}>
                        <Text style={{fontSize:28,color:'#2D3142'}}>Challenge</Text>

                    </View>
                    <TouchableOpacity onPress={this._onPressButton} >
                        <View style={{alignItems:'center',marginTop:20}}>
                            <Image source={require('../images/Card.png')}></Image>
                        </View>
                    </TouchableOpacity>


                    <View style={{marginLeft:20,marginRight:20,marginTop:10,flexDirection:'row',justifyContent:'space-between'}}>
                        <Text style={{fontSize:16,color:'#4C5980',fontWeight: 'bold'}}>Trending </Text>
                        <Image source={require('../images/more.png')}></Image>

                    </View>
                    <View style={{alignItems:'center'}}>
                        <Image source={require('../images/trending1.png')}></Image>
                    </View>
                    <View style={{alignItems:'center',}}>
                        <Image source={require('../images/trending1.png')}></Image>
                    </View>


                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({

    item: {
        backgroundColor: '#F2F2F2',
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,
    },
    title: {
        fontSize: 32,
    },
    imageWrapper: {
        height: 200,
        width: 200,
        overflow : "hidden"
    },
    theImage: {
        width: "100%",
        height: "100%",
        resizeMode: "cover",
    },
    container: {
        flex: 1,
        backgroundColor: '#F2F2F2',
        //alignItems: 'stretch',
        //justifyContent: 'center',
        //backgroundColor: '#ecf0f1',
    },
    image: {
        flexGrow:1,
        height:250,
        width:'100%',
        alignItems: 'center',
        justifyContent:'center',
    },
    paragraph: {
        margin: 24,
        fontSize: 48,
        fontWeight: 'bold',
        textAlign: 'center',
        color: 'white',
        backgroundColor: 'transparent',
    },
});
