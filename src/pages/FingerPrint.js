import React  from 'react';
import {
  SafeAreaView,
  View,
  FlatList,
  StyleSheet,
  Text,
  Image,
  ImageBackground,
  ScrollView, TouchableOpacity,TextInput
} from "react-native";


export default class FingerPrint extends React.Component {

  constructor(props) {
    super(props);
    this._onPressButton = this._onPressButton.bind(this);
  }

  _onPressButton(){

    this.props.navigation.navigate('ProfilePic');
  }

  render() {
    return (

      <SafeAreaView style={styles.container}>

        <View style={{flexDirection:'row',marginTop:40,justifyContent:'space-between'}}>
          <View>
            <Image source={require('../images/down.png')} style={{width:24,height:24,marginLeft:20}}></Image>
          </View>
          <View style={{width:119,height:8,alignItems: 'center',flexDirection:'row'}}>
            <View style={{width:59,height:8,borderRadius:5,borderColor:'#7265E3',backgroundColor:'#7265E3',marginTop:15}}></View>
            <View style={{width:59,height:8,borderRadius:5,backgroundColor:'#E1DDF5',marginTop:15}}></View>

          </View>

          <View style={{flexDirection:'row'}}>
            <Text style={{color:'#7265E3',marginRight:20,fontWeight:'bold'}}>Skip</Text>
          </View>


        </View>
        <View style={{alignItems:'center',marginTop:20}}>
          <Image style={{width:219,height:216}} source={require('../images/imga.png')}></Image>
        </View>

        <View style={{alignItems :'center',marginTop:30}}>
          <Text style={{color:'#7265E3',fontSize:12,fontWeight: 'bold'}}>STEP 1/2</Text>
        </View>
        <View style={{alignItems:'center',marginTop:20}}>
          <Text style={{color:'#2D3142',fontSize:24,fontWeight:'bold'}}>Enable Fingerprint</Text>
        </View>
        <View style={{alignItems:'center',marginTop:20,marginLeft:35,marginRight:20}}>
          <Text style={{color:'#4C5980',fontSize:16}}>If you enable touch ID, you don’t need to enter your password when you login. </Text>
        </View>
        <View style={{alignItems:'center'}}>

          <TouchableOpacity onPress={this._onPressButton} style={{
            marginTop: 70,
            flexDirection: 'row',
            justifyContent: 'center',
            backgroundColor: '#7265E3',
            paddingTop: 15,
            borderRadius: 14,
            width:250,
            height:56
          }}>
            <Text style={{color:'#ffffff',fontWeight:'bold'}}>Continue</Text>
          </TouchableOpacity>

        </View>

      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({

  item: {
    backgroundColor: '#F2F2F2',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },
  imageWrapper: {
    height: 200,
    width: 200,
    overflow : "hidden"
  },
  theImage: {
    width: "100%",
    height: "100%",
    resizeMode: "cover",
  },
  container: {
    flex: 1,
    backgroundColor: '#F2F2F2',
    //alignItems: 'stretch',
    //justifyContent: 'center',
    //backgroundColor: '#ecf0f1',
  },
  image: {
    flexGrow:1,
    height:250,
    width:'100%',
    alignItems: 'center',
    justifyContent:'center',
  },
  paragraph: {
    margin: 24,
    fontSize: 48,
    fontWeight: 'bold',
    textAlign: 'center',
    color: 'white',
    backgroundColor: 'transparent',
  },
});
