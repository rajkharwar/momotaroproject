import React from 'react';

import { Platform,Image } from 'react-native';
//import BottomDrawer from 'rn-bottom-drawer';
import {

    createAppContainer
} from 'react-navigation';
//import { createStackNavigator } from 'react-navigation-stack';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import { Ionicons } from '@expo/vector-icons';
import HomeScreen from "./HomeScreen";
import CommunityScreen from "./CommunityScreen";
import Profile from "./Profile";


import Explore from "./Explore";
import {MainStackNavigator} from "./StackNavigator";

const TabIcon = (props) => (
    <Ionicons
        name={'md-home'}
        size={20}
        color={props.focused ? '#F94702' : '#000000'}
    />
)
const SearchIcon = (props) => (
    <Ionicons
        name={'md-search'}
        size={20}
        color={props.focused ?'#F94702' : '#000000'}
    />
)
const DownloadIcon = (props) => (
    <Ionicons
        name={'md-add-circle-outline'}
        size={20}
        color={props.focused ?'#F94702' : '#000000'}
    />
)
const ShoppingIcon = (props) => (
    <Ionicons
        name={'md-card-outline'}
        size={20}
        color={props.focused ?'#F94702' : '#000000'}
    />
)
const ProfileIcon = (props) => (
    <Ionicons
        name={'md-person-outline'}
        size={20}
        color={props.focused ?'#F94702' : '#000000'}
    />
)

const TabNavigator = createBottomTabNavigator(
    {
        Discover: {
            screen: HomeScreen,
            component={MainStackNavigator},


            navigationOptions: {

                tabBarLabel: 'Discover',
                tabBarIcon: TabIcon,


                tabBarOptions: { // Here we add some options to customize our tab as default.
                    style:{
                        height:50,
                        backgroundColor: '#FFFFFF',


                    },
                    activeTintColor: '#F94702', // As the name says: The color that the active tab will have.
                    // activeBackgroundColor: '#4348AB', // The background color of the active tab
                    inactiveTintColor: '#000000', // The inactive tab color. It will be the color of the text
                    labelStyle: {// The label style. Here i just made a change on size and padding
                        fontSize: 10,


                    },


                }
            }
        },
        Explore: {
            screen: Explore,
            component={MainStackNavigator},
            navigationOptions: {
                tabBarLabel: 'Search',
                tabBarIcon: SearchIcon,
                tabBarOptions: { // Here we add some options to customize our tab as default.
                    style:{
                        height:50,
                        backgroundColor: '#FFFFFF',

                    },

                    activeTintColor: '#F94702', // As the name says: The color that the active tab will have.
                    // activeBackgroundColor: '#4348AB', // The background color of the active tab
                    inactiveTintColor: '#000000', // The inactive tab color. It will be the color of the text
                    labelStyle: {// The label style. Here i just made a change on size and padding
                        fontSize: 10,

                    },


                }

            }
        },
        CommunityScreen: {
            screen: CommunityScreen,
            component={MainStackNavigator},
            navigationOptions: {
                tabBarLabel: 'Create',
                tabBarIcon: DownloadIcon,
                tabBarOptions: { // Here we add some options to customize our tab as default.
                    style:{
                        height:50,
                        backgroundColor: '#FFFFFF',

                    },

                    activeTintColor: '#F94702', // As the name says: The color that the active tab will have.
                    // activeBackgroundColor: '#4348AB', // The background color of the active tab
                    inactiveTintColor: '#000000', // The inactive tab color. It will be the color of the text
                    labelStyle: {// The label style. Here i just made a change on size and padding
                        fontSize: 10,

                    },


                }

            }
        },
        Profile: {
            screen: Profile,
            component={MainStackNavigator},
            navigationOptions: {
                tabBarLabel: 'Shopping',
                tabBarIcon: ShoppingIcon,

                tabBarOptions: { // Here we add some options to customize our tab as default.
                    style:{
                        height:50,
                        backgroundColor: '#FFFFFF',

                    },
                    activeTintColor: '#F94702', // As the name says: The color that the active tab will have.
                    inactiveTintColor: '#000000',
                    //  activeBackgroundColor: '#4348AB', // The background color of the active tabinactiveTintColor: '#666', // The inactive tab color. It will be the color of the text
                    labelStyle: {// The label style. Here i just made a change on size and padding
                        fontSize: 10,


                    },

                }

            }
        },
    },

    {
        initialRouteName:'HomeScreen',


    },

);

export default createAppContainer(TabNavigator);




