import React, { useState, useEffect } from 'react';
import {
    SafeAreaView,
    View,
    FlatList,
    StyleSheet,
    Text,
    Image,
    CheckBox,
    ImageBackground,
    ScrollView,
    TouchableOpacity
} from 'react-native';
import Constants from 'expo-constants';
const data = [
    { id: 1, txt: 'Monday', isChecked: false },
    { id: 2, txt: 'Tuesday', isChecked: false },
    { id: 3, txt: 'Wednesday', isChecked: false },
    { id: 4, txt: 'Thrusday', isChecked: false },
    { id: 5, txt: 'Friday', isChecked: false },
    { id: 6, txt: 'Saturday', isChecked: false },
    { id: 7, txt: 'Sunday', isChecked: false },
];

export default class WeightReminder extends React.Component {

    constructor(props) {
        super(props);
        this.state = {products: data,};
        this._onPressButton = this._onPressButton.bind(this);
    }
    _onPressButton(){

        this.props.navigation.navigate('SleepReminder');
    }


    handleChange = (id) => {
        let temp = this.state.products.map((product) => {
            if (id === product.id) {
                return { ...product, isChecked: !product.isChecked };
            }
            return product;
        });
        this.setState({
            products: temp,
        });
    };
    renderFlatList = (renderData) => {
        return (
            <FlatList
                data={renderData}
                renderItem={({ item }) => (
                    <View style={{ margin: 5 }}>
                        <View style={styles.card}>
                            <View
                                style={{
                                    flexDirection: 'row',
                                    flex: 1,
                                    justifyContent: 'space-between',
                                }}>

                                <Text>{item.txt}</Text>
                                <CheckBox
                                    value={item.isChecked}
                                    onChange={() => {
                                        this.handleChange(item.id);
                                    }}
                                />
                            </View>
                        </View>
                    </View>
                )}
            />
        );
    };


    render() {
        let selected = this.state.products?.filter((product) => product.isChecked);
        return (

            <SafeAreaView style={styles.container}>
                <ScrollView>

                    <View style={{backgroundColor:'#000000' ,height:100}}>

                    </View>
                        <View>
                            <Image source={require('../images/arrowdown.png')} style={{width:24,height:24,marginLeft:20,marginTop:20}}></Image>
                        </View>

                    <View style={{alignItems:'center',marginTop:20}}>
                        <Image style={{width:300,height:200}} source={require('../images/weightReminder.png')}></Image>
                    </View>

                    <View style={{marginLeft:20,marginRight:20,marginTop:20,flexDirection:'row',justifyContent:'center'}}>
                        <Text style={{fontSize:16,color:'#4C5980',fontWeight: 'bold'}}>Weight Remainder </Text>

                    </View>
                    <View style={{marginLeft:20,marginRight:20,marginTop:20,flexDirection:'row',justifyContent:'center',alignItems: 'center'}}>
                        <Text style={{fontSize:13,color:'#4C5980'}}>Choose a day to weigh yourself and we will send you the reminder </Text>

                    </View>
                    <View style={{marginLeft:20,marginRight:20,marginTop:20,flexDirection:'row',justifyContent:'space-between'}}>
                        <Text style={{fontSize:13,color:'#7265E3'}}>SELECTED DAY (3) </Text>

                    </View>
                    <View style={styles.container}>
                        <View style={{ flex: 1 }}>
                            {this.renderFlatList(this.state.products)}
                        </View>
                    </View>

                    <View style={{alignItems:'center'}}>
                        <TouchableOpacity onPress={this._onPressButton} style={{
                            marginTop: 20,
                            flexDirection: 'row',
                            justifyContent: 'center',
                            backgroundColor: '#7265E3',
                            paddingTop: 15,
                            borderRadius: 14,
                            width:250,
                            height:56
                        }}>
                            <Text style={{color:'#ffffff',fontWeight:'bold'}}>Set Reminder</Text>
                        </TouchableOpacity>
                    </View>


                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        //alignItems: 'stretch',
        //justifyContent: 'center',
        paddingTop: Constants.statusBarHeight,
        //backgroundColor: '#ecf0f1',
    },
    image: {
        //flexGrow:1,
        height:250,
        width:'100%',
        alignItems: 'center',
        justifyContent:'center',
    },
    paragraph: {
        margin: 24,
        fontSize: 48,
        fontWeight: 'bold',
        textAlign: 'center',
        color: 'white',
        backgroundColor: 'transparent',
    },
    card: {
        padding: 10,
        margin: 5,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    modalView: {
        margin: 20,
        backgroundColor: 'white',
        borderRadius: 20,
        padding: 5,
        justifyContent: 'space-between',
        alignItems: 'center',
        elevation: 5,
    },
    text: {
        textAlign: 'center',
        fontWeight: 'bold',
    },
});
