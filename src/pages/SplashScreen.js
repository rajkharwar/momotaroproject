import React, {Component} from 'react';
import {
  StatusBar,
  View,
  Text,
  StyleSheet,
  ActivityIndicator,
  Image,
  AsyncStorage,
  TouchableOpacity,
} from 'react-native';

export default class SplashScreen extends React.Component {
  constructor(props) {
    super(props);
    this._onPressButton = this._onPressButton.bind(this);
  }

  _onPressButton() {
    this.props.navigation.navigate('MobileNumber');
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={{flexDirection: 'column', alignItems: 'center'}}>
          <Image
            source={require('../images/icons.png')}
            style={{width: 68, height: 68, marginTop: 30}}
          />
          <View style={{flexDirection: 'column'}}>
            <View style={{flexDirection: 'column', alignItems: 'center'}}>
              <Text style={{color: '#2D3142', fontSize: 32, font: 'rubik'}}>
                Welcome to
              </Text>
              <View style={{flexDirection: 'row'}}>
                <Text style={{color: '#7265E3', fontSize: 32, font: 'rubik'}}>
                  Momotaro
                </Text>

                <Text style={{color: '#2D3142', fontSize: 32, font: 'rubik'}}>
                  {' '}
                  UI Kit
                </Text>
              </View>
            </View>
            <View
              style={{
                flexDirection: 'column',
                alignItems: 'center',
                marginTop: 5,
              }}>
              <Text
                style={{color: '#2D3142', fontSize: 16, font: 'Rubik-Regular'}}>
                The best UI for your next
              </Text>

              <Text
                style={{color: '#2D3142', fontSize: 16, font: 'Rubik-Regular'}}>
                health and fitness project
              </Text>
            </View>
            <Image
              source={require('../images/image.png')}
              style={{height: 297, marginTop: 20}}
            />
          </View>

          <TouchableOpacity
            onPress={this._onPressButton}
            style={{
              marginTop: 30,
              flexDirection: 'row',
              justifyContent: 'center',
              backgroundColor: '#7265E3',
              paddingTop: 15,
              borderRadius: 14,
              width: 250,
              height: 56,
            }}>
            <Text style={{color: '#ffffff', fontWeight: 'bold'}}>
              Get Started
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

var styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F2F2F2',
    alignItems: 'center',
  },

  logo: {
    //  backgroundColor: 'rgba(0,0,0,0)',
    // width: 100,
    // height: 400,
    marginTop: 80,
  },
  backdrop: {
    flex: 1,
    flexDirection: 'column',
  },
  headline: {
    fontSize: 18,
    textAlign: 'center',
    backgroundColor: 'black',
    color: 'white',
  },
  textStyle: {
    fontSize: 25,
    color: '#000000',
    fontWeight: 'bold',
    fontFamily: 'sans-serif-light',
    alignItems: 'center',
  },
  Forgot: {
    color: '#000000',
    fontSize: 13,
    textAlign: 'center',
    justifyContent: 'center',
  },
  FacebookStyle: {
    flexDirection: 'column',
    alignItems: 'center',
    paddingHorizontal: 17,
    height: 70,
    //width: 100,
  },
  GooglePlusStyle: {
    flexDirection: 'column',
    alignItems: 'center',
    paddingHorizontal: 17,
    height: 70,
    //width: 100,
  },
  PayBox: {
    marginTop: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});
