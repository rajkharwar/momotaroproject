import React  from 'react';
import {
    SafeAreaView,
    View,
    FlatList,
    StyleSheet,
    Text,
    Image,
    ImageBackground,
    ScrollView, TouchableOpacity, TextInput, CheckBox
} from "react-native";
const DATA = [
    {
        id:1,
        image: require('../images/Bg1.png'),
        title:'Danny',
        desc:'Joined',
        text:'$5',


    },
    {
        id:2,
        image: require('../images/Bg1.png'),
        title:'Danny',
        desc:'Joined',
        text:'$5',

    },
];

export default class InviteScreen extends React.Component {

    constructor(props) {
        super(props);
        this._onPressButton = this._onPressButton.bind(this);
    }

    _onPressButton(){

        this.props.navigation.navigate('NotificationScreen');
    }

    render() {
        return (

            <SafeAreaView style={styles.container}>

                <ScrollView>

                    <View style={{flexDirection:'row', marginTop:70,marginHorizontal:10,alignItems:'center'}}>
                        <TouchableOpacity  onPress={() => this._onPressBackButton()}>
                            <Image  source={require('../images/down.png')} />
                        </TouchableOpacity>
                        <View style={{flex: 1,
                            justifyContent: "center",
                            alignItems: "center"}}>
                            <Text style={{color:'#2D3142',fontSize:18,fontWeight:'bold'}}>Share</Text>
                        </View>
                    </View>
                    <TouchableOpacity onPress={this._onPressButton}>
                    <View style={{alignItems:'center',marginTop:20}}>
                        <Image source={require('../images/inviteScreen.png')}></Image>
                    </View>
                    </TouchableOpacity>

                    <View style={{marginTop:20,alignItems: 'center'}}>
                        <Text style={{color:'#2D3142',fontSize:20,fontWeight:'bold'}}>Get $5 off</Text>
                    </View>
                    <View style={{marginTop:10,alignItems: 'center'}}>
                        <Text style={{color:'#4C5980',fontSize:15}}>Each time a friend signs up through</Text>
                            <Text style={{color:'#4C5980',fontSize:15}}> your refereral link we’ll reward you both $5</Text>
                    </View>
                    <View style={{alignItems:'center',marginTop:20}}>
                        <Image source={require('../images/CombinedShape.png')}></Image>
                    </View>
                    <View style={{marginTop:20,alignItems: 'center'}}>
                        <Text style={{color:'#4C5980',fontSize:15}}>Or Send Invite</Text>
                    </View>
                    <View style={{
                        flexDirection: 'row',
                        marginLeft: 15,
                        marginTop: 30,
                        // justifyContent: 'space-between'
                    }}>

                            <View style={{flexDirection: 'column', marginLeft: 15}}>

                                <Image  source={require('../images/squareCopy.png')} />
                            </View>
                        <View style={{flexDirection: 'column',marginLeft: 25,paddingVertical:10}}>
                            <Text style={{color:'#7265E3',fontSize:18}}>Add Friend</Text>
                    </View>
                    </View>
                    <View>
                    <FlatList
                        numColumns={1}
                        data={DATA}
                        keyExtriactor={(time, index) => index.toString()}
                        renderItem={({item}) => {
                            return (
                                <View style={styles.listItem}>
                                    <Image source={item.image}  style={{width:60, height:60,borderRadius:30}} />
                                    <View style={{marginLeft:20,flex:1}}>
                                        <Text style={{fontWeight:"bold"}}>{item.title}</Text>
                                        <Text>{item.desc}</Text>
                                    </View>
                                    <View  style={{marginRight: 10,flexDirection: 'row',
                                        justifyContent: 'center',
                                        backgroundColor: '#E4DFFF',
                                        paddingTop: 10,
                                        borderRadius: 14,
                                        width:50,
                                        height:40,}}>
                                        <Text style={{color:'#7265E3'}}>{item.text}</Text>

                                    </View>
                                </View>

                            );
                        }}
                    />
                </View>

                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: '#F2F2F2',
    },
    listItem:{
        margin:10,
        //  padding:10,
        width:"90%",
        flex:1,
        alignSelf:"center",
        flexDirection:"row",
        borderRadius:5
    }


});
