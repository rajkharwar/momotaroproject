import React  from 'react';
import {
    SafeAreaView,
    View,
    FlatList,
    StyleSheet,
    Text,
    Image,
    ImageBackground,
    ScrollView,
    TouchableOpacity
} from 'react-native';
import Constants from 'expo-constants';
const DATA = [
    {
        id:1,
        image: require('../images/calories.png'),
        title: 'Salad with wheat and white egg',
        desc: '200 cals',

    },
    {
        id:2,
        image: require('../images/calories.png'),
        title: 'Salad with wheat and white egg',
        desc: '200 cals',

    },
    {
        id:3,
        image: require('../images/calories.png'),
        title: 'Salad with wheat and white egg',
        desc: '200 cals',

    },
    {
        id:4,
        image: require('../images/calories.png'),
        title: 'Salad with wheat and white egg',
        desc: '200 cals',

    },
];


export default class JournalScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = {products: DATA};
        this._onPressButton = this._onPressButton.bind(this);
    }

    _onPressButton(){

        this.props.navigation.navigate('NutritionDetailScreen');
    }

    handleChange = (id) => {
        let temp = this.state.products.map((product) => {
            if (id === product.id) {
                return { ...product, isChecked: !product.isChecked };
            }
            return product;
        });
        this.setState({
            products: temp,
        });
    };


    renderFlatList = (renderData) => {
        return (
            <FlatList
                data={renderData}
                renderItem={({ item }) => (

                        <View style={styles.listItem}>
                            <Image source={item.image}  style={{width:60, height:60,borderRadius:30}} />
                            <View style={{alignItems:"center",flex:1}}>
                                <Text style={{fontWeight:"bold"}}>{item.title}</Text>
                                <Text>{item.desc}</Text>
                            </View>
                    </View>
                )}
            />
        );
    };


    render() {
        return (

            <SafeAreaView style={styles.container}>
                <ScrollView>

                    <View style={{backgroundColor:'#7265E3' ,height:180}}>
                        <View style={{flexDirection:'row', marginTop:20,marginHorizontal:10,alignItems:'center'}}>
                            <TouchableOpacity  onPress={() => this._onPressBackButton()}>
                                <Image  source={require('../images/arrow.png')} />
                            </TouchableOpacity>
                            <View style={{flex: 1,
                                justifyContent: "center",
                                alignItems: "center"}}>
                                <Text style={{color:'#ffffff',fontSize:15}}>Journal</Text>
                            </View>
                        </View>

                    </View>
                    <View style={{flexDirection:'row',backgroundColor:'#FFFFFF',height:45,marginLeft:20,marginRight:20, justifyContent:'center', borderRadius:15, marginTop:20}}>
                        <Image source={require('../images/search.png')} style={{marginTop:15,marginLeft:10}}></Image>
                        <Text style={{color:'#969490',fontSize:15,marginTop:13,marginLeft:5}}>Search Meal</Text>
                    </View>

                    <View style={{backgroundColor:'#FFFFFF',marginTop:20,borderRadius:12,marginLeft:20,marginRight:20,paddingBottom:20}}>
                    <View style={{
                        flexDirection: 'row',
                        marginLeft: 15,
                        marginTop: 20,
                        justifyContent: 'space-between'
                    }}>
                        <View style={{flexDirection: 'column', marginLeft: 5}}>
                            <Text style={{color: "#7265E3",fontSize:15}}>BREAKFAST</Text>
                            <View style={{flexDirection:'row', marginTop:10,alignItems:'center'}}>

                                    <Image  source={require('../images/Shape.png')} />

                                <View style={{flex: 1,
                                    justifyContent: "center",
                                    alignItems: "center"}}>
                                    <Text style={{color:'#7265E3',fontSize:10}}>120 kcl/450kcl</Text>
                                </View>
                            </View>

                        </View>
                        <View  style={{marginRight: 10,}}>
                            <TouchableOpacity onPress={this._onPressButton} style={{

                                flexDirection: 'row',
                                justifyContent: 'center',
                                backgroundColor: '#E4DFFF',
                                paddingTop: 8,
                                borderRadius: 14,
                                width:50,
                                height:50,
                            }}>
                                <Image  source={require('../images/Add.png')} />
                            </TouchableOpacity>

                        </View>
                    </View>
                        <View>
                                {this.renderFlatList(this.state.products)}
                            </View>
                        <TouchableOpacity onPress={this._onPressButtons} >
                            <View style={{alignItems:'center',marginTop:20}}>
                                <Image source={require('../images/Comment.png')}></Image>
                            </View>
                        </TouchableOpacity>
                        </View>

                    <View style={{backgroundColor:'#FFFFFF',marginTop:20,borderRadius:12,marginLeft:20,marginRight:20,paddingBottom:20}}>
                        <View style={{
                            flexDirection: 'row',
                            marginLeft: 15,
                            marginTop: 20,
                            justifyContent: 'space-between'
                        }}>
                            <View style={{flexDirection: 'column', marginLeft: 5}}>
                                <Text style={{color: "#7265E3",fontSize:15}}>LUNCH</Text>
                                <View style={{flexDirection:'row', marginTop:10,alignItems:'center'}}>

                                    <Image  source={require('../images/Shape.png')} />

                                    <View style={{flex: 1,
                                        justifyContent: "center",
                                        alignItems: "center"}}>
                                        <Text style={{color:'#7265E3',fontSize:10}}>120 kcl/450kcl</Text>
                                    </View>
                                </View>

                            </View>
                            <View  style={{marginRight: 10,}}>
                                <TouchableOpacity onPress={this._onPressButtons} style={{

                                    flexDirection: 'row',
                                    justifyContent: 'center',
                                    backgroundColor: '#E4DFFF',
                                    paddingTop: 8,
                                    borderRadius: 14,
                                    width:50,
                                    height:50,
                                }}>
                                    <Image  source={require('../images/Add.png')} />
                                </TouchableOpacity>

                            </View>
                        </View>
                        <View>
                            {this.renderFlatList(this.state.products)}
                        </View>
                        <TouchableOpacity onPress={this._onPressButtons} >
                            <View style={{alignItems:'center',marginTop:20}}>
                                <Image source={require('../images/Comment.png')}></Image>
                            </View>
                        </TouchableOpacity>
                    </View>

                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({

    item: {
        backgroundColor: '#ffffff',
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,

    },
    title: {
        fontSize: 32,
    },

    container: {
        flex: 1,
        //alignItems: 'stretch',
        //justifyContent: 'center',
        paddingTop: Constants.statusBarHeight,
        //backgroundColor: '#ecf0f1',
    },
    image: {
        //flexGrow:1,
        height:250,
        width:'100%',
        alignItems: 'center',
        justifyContent:'center',
    },
    listItem:{
        margin:10,
      //  padding:10,
        backgroundColor:"#FFF",
        width:"80%",
        flex:1,
        alignSelf:"center",
        flexDirection:"row",
        borderRadius:5
    }
});
