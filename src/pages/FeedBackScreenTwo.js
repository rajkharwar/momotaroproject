import React  from 'react';
import {
    SafeAreaView,
    View,
    FlatList,
    StyleSheet,
    Text,
    Image,
    ImageBackground,
    ScrollView,
    TouchableOpacity,
    TextInput
} from 'react-native';
import Constants from 'expo-constants';
import AppIntroSlider from "react-native-app-intro-slider";




const rowHeight = 45;


export default class FeedBackScreenTwo extends React.Component {

    constructor(props) {
        super(props);
        this._onPressButton = this._onPressButton.bind(this);
    }

    _onPressButton(){

        this.props.navigation.navigate('CreatePostScreen');
    }



    render() {
        return (

            <SafeAreaView style={styles.container}>
                <ScrollView>

                    <View style={{backgroundColor:'#8C80F8' ,height:180}}>
                        <View style={{alignItems:'center',marginTop:50}}>
                            <Image source={require('../images/downCopy.png')}></Image>
                        </View>
                    </View>

                    <View style={{flexGrow: 1,alignItems: 'center'}}>
                        <Image
                            source={require('../images/smiley.png')}
                            style={{
                                position: 'absolute',
                                top: -70,
                                height: 120,
                                width: 150}}
                        />
                    </View>
                    <View style={{alignItems: 'center',marginTop:50}}>
                        <View style={{marginLeft:30,marginTop:20}}>
                            <Text style={{color: '#2D3142',fontSize:18,fontWeight: 'bold'}}>
                                Which area that{'\n'}

                                <Text style={{color: '#2D3142',fontSize:18,fontWeight: 'bold'}}>
                                    need improvement?
                                </Text>
                            </Text>
                        </View>
                    </View>

                    <View style={{flexDirection:'row', marginTop:20,marginHorizontal:10,marginLeft:15}}>
                        <TouchableOpacity onPress={this._onPressButton} style={{
                            flexDirection: 'row',
                            justifyContent: 'center',
                            backgroundColor: '#7265E3',
                            paddingTop: 15,
                            borderRadius: 14,
                            width:170,
                            height:56
                        }}>
                            <Text style={{color:'#FFFFFF',fontWeight:'bold'}}>Add To Journal</Text>
                        </TouchableOpacity>
                        <View style={{flex: 1,justifyContent:'center',alignItems:'center'}}>
                            <TouchableOpacity style={{
                                flexDirection: 'row',
                                justifyContent: 'center',
                                backgroundColor: '#E4DFFF',
                                paddingTop: 15,
                                borderRadius: 14,
                                width:170,
                                height:56,
                            }}>
                                <Text style={{color:'#FFFFFF',fontWeight:'bold'}}>Customer Service</Text>
                            </TouchableOpacity>
                        </View>
                    </View>


                    <View style={{flexDirection:'row', marginTop:20,marginHorizontal:10,marginLeft:30}}>
                        <TouchableOpacity onPress={this._onPressButton} style={{
                            flexDirection: 'row',
                            justifyContent: 'center',
                            backgroundColor: '#E4DFFF',
                            paddingTop: 15,
                            borderRadius: 14,
                            width:150,
                            height:56
                        }}>
                            <Text style={{color:'#FFFFFF',fontWeight:'bold'}}>App Crash</Text>
                        </TouchableOpacity>
                        <View style={{flex: 1,justifyContent:'center',alignItems:'center'}}>
                            <TouchableOpacity style={{
                                flexDirection: 'row',
                                justifyContent: 'center',
                                backgroundColor: '#E4DFFF',
                                paddingTop: 15,
                                borderRadius: 14,
                                width:150,
                                height:56,
                            }}>
                                <Text style={{color:'#FFFFFF',fontWeight:'bold'}}>Navigation</Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={{flexDirection:'row', marginTop:20,marginHorizontal:10,marginLeft:15}}>
                        <TouchableOpacity onPress={this._onPressButton} style={{
                            flexDirection: 'row',
                            justifyContent: 'center',
                            backgroundColor: '#8FACFF',
                            paddingTop: 15,
                            borderRadius: 14,
                            width:170,
                            height:56
                        }}>
                            <Text style={{color:'#FFFFFF',fontWeight:'bold'}}>Not Responsive</Text>
                        </TouchableOpacity>
                        <View style={{flex: 1,justifyContent:'center',alignItems:'center'}}>
                            <TouchableOpacity style={{
                                flexDirection: 'row',
                                justifyContent: 'center',
                                backgroundColor: '#1F87FE',
                                paddingTop: 15,
                                borderRadius: 14,
                                width:170,
                                height:56,
                            }}>
                                <Text style={{color:'#FFFFFF',fontWeight:'bold'}}>Not Functional</Text>
                            </TouchableOpacity>
                        </View>
                    </View>


                    <View style={{marginTop:20,flex: 1,justifyContent:'center',alignItems:'center'}}>
                        <TouchableOpacity style={{
                            flexDirection: 'row',
                            justifyContent: 'center',
                            backgroundColor: '#FF9B90',
                            paddingTop: 10,
                            borderRadius: 14,
                            width:120,
                            height:46,
                        }}>
                            <Text style={{color:'#FFFFFF',fontWeight:'bold'}}>Security Issue</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{flexDirection:'row', marginTop:20,marginHorizontal:10,marginLeft:15}}>
                        <TouchableOpacity onPress={this._onPressButton} style={{
                            flexDirection: 'row',
                            justifyContent: 'center',
                            backgroundColor: '#F4F6FA',
                            paddingTop: 15,
                            borderRadius: 14,
                            width:170,
                            height:56
                        }}>
                            <Text style={{color:'#9C9EB9',fontWeight:'bold'}}>Slow Loading</Text>
                        </TouchableOpacity>
                        <View style={{flex: 1,justifyContent:'center',alignItems:'center'}}>
                            <TouchableOpacity style={{
                                flexDirection: 'row',
                                justifyContent: 'center',
                                backgroundColor: '#F4F6FA',
                                paddingTop: 15,
                                borderRadius: 14,
                                width:170,
                                height:56,
                            }}>
                                <Text style={{color:'#9C9EB9',fontWeight:'bold'}}>Customer Service</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={{alignItems:'center'}}>
                        <TouchableOpacity onPress={this._onPressButton} style={{
                            marginTop: 20,
                            flexDirection: 'row',
                            justifyContent: 'center',
                            backgroundColor: '#7265E3',
                            // paddingTop: 15,
                            borderRadius: 14,
                            width:250,
                            height:56
                        }}>
                            <View style={{flexDirection:'row',alignItems:'center'}}>

                                <Image  source={require('../images/ShapeLetter.png')} style={{
                                    height: 10,
                                    width: 15}} />

                                <View style={{marginLeft:20}}>
                                    <Text style={{color:'#FFFFFF',fontSize:18}}>Submit</Text>
                                </View>
                            </View>
                        </TouchableOpacity>
                    </View>


                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({

    item: {
        backgroundColor: '#ffffff',
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,

    },
    title: {
        fontSize: 32,
    },
    imageWrapper: {
        height: 200,
        width: 200,
        overflow : "hidden"
    },
    theImage: {
        width: "100%",
        height: "100%",
        resizeMode: "cover",
    },
    container: {
        flex: 1,
        //alignItems: 'stretch',
        //justifyContent: 'center',
        paddingTop: Constants.statusBarHeight,
        backgroundColor: '#ffffff',
    },

});
