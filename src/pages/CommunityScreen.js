import React  from 'react';
import {
    SafeAreaView,
    View,
    FlatList,
    StyleSheet,
    Text,
    Image,
    ImageBackground,
    ScrollView, TouchableOpacity, TextInput, CheckBox
} from "react-native";
const DATA = [
    {
        id:1,
        image: require('../images/CommunityName.png'),


    },
    {
        id:2,
        image: require('../images/CommunityName.png'),

    },
    {
        id:3,
        image: require('../images/CommunityName.png'),


    },
    {
        id:4,
        image: require('../images/CommunityName.png'),


    },
];
const LIST = [
    {
        id:1,
        image: require('../images/Bg1.png'),
        title:'Danny',
        desc:'Joined',
        text:'$5',


    },
    {
        id:2,
        image: require('../images/Bg1.png'),
        title:'Danny',
        desc:'Joined',
        text:'$5',

    },
];

export default class CommunityScreen extends React.Component {

    constructor(props) {
        super(props);
        this._onPressButton = this._onPressButton.bind(this);
    }

    _onPressButton(){

        this.props.navigation.navigate('PollsDetailScreen');
    }

    render() {
        return (

            <SafeAreaView style={styles.container}>

                <ScrollView>

                    <View style={{
                        flexDirection: 'row',
                        marginLeft: 20,
                        marginTop: 50,}}>
                        <TouchableOpacity  onPress={() => this._onPressBackButton()}>
                            <Image  source={require('../images/down.png')} />
                        </TouchableOpacity>
                    </View>
                    <View style={{marginTop:20, marginLeft: 20}}>
                        <Text style={{color:'#2D3142',fontSize:25,fontWeight:'bold'}}>Community</Text>
                    </View>
                    <View>
                        <FlatList
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}
                            data={DATA}
                            keyExtriactor={(time, index) => index.toString()}
                            renderItem={({item}) => {
                                return (
                                    <View style={{width: 100,height: 100,marginLeft: 10,marginTop: 20}}>
                                        <Image source={item.image} style={{height: 70,width:80}}>
                                        </Image>
                                    </View>


                                );
                            }}
                        />
                    </View>
                    <View style={{backgroundColor:'#FFFFFF',borderTopRightRadius:40,borderTopLeftRadius:40,height: 1500}}>

                        <View style={{
                            flexDirection: 'row',
                            marginLeft: 15,
                            marginTop: 20,
                            //justifyContent: 'space-between'
                        }}>
                            <View style={{flexDirection: 'column', marginLeft: 5}}>
                                <TouchableOpacity  onPress={() => this._onPressButton()}>
                                    <Image  source={require('../images/avatar.png')} />
                                </TouchableOpacity>
                            </View>
                            <View style={{flexDirection: 'column', marginLeft: 5}}>
                                <Text style={{color:'#000000',fontWeight:'bold'}}>Jessica mendis</Text>
                                <Text>food 10 min ago</Text>
                            </View>
                            <View  style={{marginLeft:160}}>
                                <TouchableOpacity onPress={this._onPressButton} style={{
                                    width:70,
                                    height:46,
                                }}>
                                    <Image  source={require('../images/more.png')} />

                                </TouchableOpacity>

                            </View>
                        </View>
                        <View style={{flexDirection:'row', marginTop:20,alignItems:'center',justifyContent:'center'}}>

                            <Text style={{color:'#2D3142',fontSize:15,fontWeight:'bold'}}>Yummy breakfast on weekend {''}</Text>
                            <Text style={{color:'#7265E3',fontSize:15,fontWeight:'bold'}}>#brekkie</Text>
                        </View>
                        <View style={{marginLeft:20,marginTop:20}}>
                            <Image source={require('../images/PhotoCom.png')}></Image>
                        </View>
                        <View style={{flexDirection:'row',marginLeft:20}}>
                        <View style={{alignItems:'center',marginTop:20}}>
                            <Image source={require('../images/PhotoCom2.png')}></Image>
                        </View>
                            <View style={{marginTop:20,marginLeft:20}}>
                                <Image source={require('../images/PhotoCom2.png')}></Image>
                            </View>
                        </View>
                        <View style={{
                            flexDirection: 'row',
                            marginLeft: 15,
                            marginTop: 20,
                            // justifyContent: 'space-between'
                        }}>
                            <View style={{flexDirection: 'column', marginLeft: 15}}>

                                <Image  source={require('../images/heart1.png')} />
                            </View>
                            <View style={{flexDirection: 'column', marginLeft: 15}}>
                                <Text style={{color:'#2D3142',fontWeight:'bold'}}>45</Text>

                            </View>
                            <View style={{flexDirection: 'column', marginLeft: 15}}>

                                <Image  source={require('../images/comment1.png')} />
                            </View>
                            <View style={{flexDirection: 'column', marginLeft: 15}}>
                                <Text style={{color:'#2D3142',fontWeight:'bold'}}>4</Text>

                            </View>
                            <View style={{ flexDirection: 'row',marginLeft: 'auto'}}>
                                <View style={{flexDirection: 'column'}}>
                                    <Image  source={require('../images/share1.png')} />
                                </View>
                                <View style={{flexDirection: 'column',marginLeft:10,marginRight:20}}>
                                    <Text style={{color:'#7265E3',fontWeight:'bold'}}>Share</Text>
                                </View>
                            </View>
                        </View>

                        <View style={{
                            flexDirection: 'row',
                            marginLeft: 15,
                            marginTop: 20,
                            //justifyContent: 'space-between'
                        }}>
                            <View style={{flexDirection: 'column', marginLeft: 5}}>
                                <TouchableOpacity  onPress={() => this._onPressBackButton()}>
                                    <Image  source={require('../images/avatar.png')} />
                                </TouchableOpacity>
                            </View>
                            <View style={{flexDirection: 'column', marginLeft: 5}}>
                                <Text style={{color:'#000000',fontWeight:'bold'}}>Sebestian</Text>
                                <Text>Achievement 10 min ago</Text>
                            </View>
                            <View  style={{marginLeft:160}}>
                                <TouchableOpacity onPress={this._onPressButton} style={{
                                    width:70,
                                    height:46,
                                }}>
                                    <Image  source={require('../images/more.png')} />

                                </TouchableOpacity>

                            </View>
                        </View>
                        <View style={{backgroundColor:'#D6D9E0',borderTopRightRadius:30,borderRadius:25,height: 300,marginLeft:30,marginRight:30,marginTop:20}}>
                            <View style={{flexDirection:'row', marginTop:30,marginLeft:25}}>
                                <Text style={{color:'#7265E3',fontSize:12,fontWeight:'bold'}}>LEADERBOARD</Text>
                            </View>
                            <View style={{flexDirection:'row', marginTop:10,marginLeft:25}}>
                                <Text style={{color:'#2D3142',fontSize:12,fontWeight:'bold'}}>You are on 1st place among your friends</Text>
                            </View>
                            <View>
                                <FlatList
                                    numColumns={1}
                                    data={LIST}
                                    keyExtriactor={(time, index) => index.toString()}
                                    renderItem={({item}) => {
                                        return (
                                            <View style={styles.listItem}>
                                                <Image source={item.image}  style={{width:40, height:40,borderRadius:30}} />
                                                <View style={{marginLeft:20,flex:1}}>
                                                    <Text style={{fontWeight:"bold"}}>{item.title}</Text>
                                                    <Text>{item.desc}</Text>
                                                </View>
                                                <View  style={{marginRight: 10,flexDirection: 'row',
                                                    justifyContent: 'center',
                                                    backgroundColor: '#E4DFFF',
                                                    paddingTop: 10,
                                                    borderRadius: 14,
                                                    width:50,
                                                    height:40,}}>
                                                    <Text style={{color:'#7265E3'}}>{item.text}</Text>

                                                </View>
                                            </View>

                                        );
                                    }}
                                />
                            </View>
                        </View>
                        <View style={{
                            flexDirection: 'row',
                            marginLeft: 15,
                            marginTop: 20,
                            // justifyContent: 'space-between'
                        }}>
                            <View style={{flexDirection: 'column', marginLeft: 15}}>

                                <Image  source={require('../images/heart1.png')} />
                            </View>
                            <View style={{flexDirection: 'column', marginLeft: 15}}>
                                <Text style={{color:'#2D3142',fontWeight:'bold'}}>45</Text>

                            </View>
                            <View style={{flexDirection: 'column', marginLeft: 15}}>

                                <Image  source={require('../images/comment1.png')} />
                            </View>
                            <View style={{flexDirection: 'column', marginLeft: 15}}>
                                <Text style={{color:'#2D3142',fontWeight:'bold'}}>4</Text>

                            </View>
                            <View style={{ flexDirection: 'row',marginLeft: 'auto'}}>
                                <View style={{flexDirection: 'column'}}>
                                    <Image  source={require('../images/share1.png')} />
                                </View>
                                <View style={{flexDirection: 'column',marginLeft:10,marginRight:20}}>
                                    <Text style={{color:'#7265E3',fontWeight:'bold'}}>Share</Text>
                                </View>
                            </View>
                        </View>
                        <View style={{
                            flexDirection: 'row',
                            marginLeft: 15,
                            marginTop: 20,
                            //justifyContent: 'space-between'
                        }}>
                            <View style={{flexDirection: 'column', marginLeft: 5}}>
                                <TouchableOpacity  onPress={() => this._onPressBackButton()}>
                                    <Image  source={require('../images/avatar.png')} />
                                </TouchableOpacity>
                            </View>
                            <View style={{flexDirection: 'column', marginLeft: 5}}>
                                <Text style={{color:'#000000',fontWeight:'bold'}}>John</Text>
                                <Text>Walk 10 min ago</Text>
                            </View>
                            <View  style={{marginLeft:160}}>
                                <TouchableOpacity onPress={this._onPressButton} style={{
                                    width:70,
                                    height:46,
                                }}>
                                    <Image  source={require('../images/more.png')} />

                                </TouchableOpacity>

                            </View>
                        </View>
                        <View style={{marginTop:20,alignItems:'center'}}>
                            <Image source={require('../images/AverageSteps.png')}></Image>
                        </View>
                        <View style={{
                            flexDirection: 'row',
                            marginLeft: 15,
                            marginTop: 20,
                            // justifyContent: 'space-between'
                        }}>
                            <View style={{flexDirection: 'column', marginLeft: 15}}>

                                <Image  source={require('../images/heart1.png')} />
                            </View>
                            <View style={{flexDirection: 'column', marginLeft: 15}}>
                                <Text style={{color:'#2D3142',fontWeight:'bold'}}>45</Text>

                            </View>
                            <View style={{flexDirection: 'column', marginLeft: 15}}>

                                <Image  source={require('../images/comment1.png')} />
                            </View>
                            <View style={{flexDirection: 'column', marginLeft: 15}}>
                                <Text style={{color:'#2D3142',fontWeight:'bold'}}>4</Text>

                            </View>
                            <View style={{ flexDirection: 'row',marginLeft: 'auto'}}>
                                <View style={{flexDirection: 'column'}}>
                                    <Image  source={require('../images/share1.png')} />
                                </View>
                                <View style={{flexDirection: 'column',marginLeft:10,marginRight:20}}>
                                    <Text style={{color:'#7265E3',fontWeight:'bold'}}>Share</Text>
                                </View>
                            </View>
                        </View>
                    </View>

                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: '#F2F2F2',
    },
    listItem:{
        margin:10,
        padding:10,
        width:"90%",
        backgroundColor: '#FFFFFF',
        flex:1,
        alignSelf:"center",
        flexDirection:"row",
        borderRadius:5
    }

});
