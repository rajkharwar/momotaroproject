import React, { useState } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    StatusBar,
    Keyboard,
    ActivityIndicator,
    Platform,
    Image,
} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';

import {
    createStackNavigator,
    TransitionSpecs,
    HeaderStyleInterpolators,
    CardStackStyleInterpolator,
    CardStyleInterpolators
} from '@react-navigation/stack';
import SplashScreen from "./SplashScreen";
import MobileNumber from "./MobileNumber";
import VerifyMobile from "./VerifyMobile";
import SetPassword from "./SetPassword";
import FingerPrint from "./FingerPrint";
import ProfilePic from "./ProfilePic";
import Interest from "./Interest";
import GoalScreen from "./GoalScreen";
import Gender from "./Gender";
import LevelScreen from "./LevelScreen";
import Notification from "./Notification";
import WeightScreen from "./WeightScreen";
import HomeScreen from "./HomeScreen";
import Wellness from "./Wellness";
import Explore from "./Explore";
import Challenges from "./Challenges";
import Profile from "./Profile";
import Progress from "./Progress"
import WeightReminder from "./WeightReminder";
import SleepReminder from "./SleepReminder";
import HomeProfile from "./HomeProfile";
import Nutrition from "./Nutrition";
import JournalScreen from "./JournalScreen";
import RecipeScreen from "./RecipeScreen";
import StepScreen from "./StepScreen";
import NutritionDetailScreen from "./NutritionDetailScreen";
import StepInsightScreen from "./StepInsightScreen";
import WaterScreen from "./WaterScreen";
import ShareScreen from "./ShareScreen";
import CommunityScreen from "./CommunityScreen";
import PollsDetailScreen from "./PollsDetailScreen";
import CreatePostScreen from "./CreatePostScreen";
import InviteScreen from "./InviteScreen";
import NotificationScreen from "./NotificationScreen";
import FeedBackScreenOne from "./FeedBackScreenOne";
import FeedBackScreenTwo from "./FeedBackScreenTwo";



// function SplashScreen () {
/* Component state. Change someState, setEnteredSomeState and state object as per your scenario. */
//   const [someState, setEnteredSomeState] = useState({
//     state1: '',
//     state2: '',
//     state3: false,
//   });
//   return (
//       <View style={styles.container}>
//         {/* Set statusbar background color and style so it blends with the screen */}
//         <StatusBar backgroundColor="#fff" barStyle="dark-content" />
//         <Text style={styles.label}>SignUp  will show here</Text>
//       </View>
//   );
// }

const Stack = createStackNavigator();
const MainStackNavigator = () => {
    return (

            <Stack.Navigator
                initialRouteName="SplashScreen"
                screenOptions={{
                    headerShown: false,
                    headerTitleAlign: 'center',
                    headerMode: 'screen',
                }}>
                <Stack.Screen
                    name="SplashScreen"
                    component={SplashScreen}
                    options={{ headerShown: false }}
                />
                <Stack.Screen
                    name="MobileNumber"
                    component={MobileNumber}
                    options={{ headerShown: false }}
                />
                <Stack.Screen
                    name="VerifyMobile"
                    component={VerifyMobile}
                    options={{ headerShown: false }}
                />
                <Stack.Screen
                    name="SetPassword"
                    component={SetPassword}
                    options={{ headerShown: false }}
                />
                <Stack.Screen
                    name="FingerPrint"
                    component={FingerPrint}
                    options={{ headerShown: false }}
                />
                <Stack.Screen
                    name="ProfilePic"
                    component={ProfilePic}
                    options={{ headerShown: false }}
                />
                <Stack.Screen
                    name="Interest"
                    component={Interest}
                    options={{ headerShown: false }}
                />
                <Stack.Screen
                    name="GoalScreen"
                    component={GoalScreen}
                    options={{ headerShown: false }}
                />
                <Stack.Screen
                    name="Gender"
                    component={Gender}
                    options={{ headerShown: false }}
                />
                <Stack.Screen
                    name="LevelScreen"
                    component={LevelScreen}
                    options={{ headerShown: false }}
                />
                <Stack.Screen
                    name="Notification"
                    component={Notification}
                    options={{ headerShown: false }}
                />
                <Stack.Screen
                    name="WeightScreen"
                    component={WeightScreen}
                    options={{ headerShown: false }}
                />
                <Stack.Screen
                    name="HomeScreen"
                    component={HomeScreen}
                    options={{ headerShown: false }}
                />
                <Stack.Screen
                    name="Wellness"
                    component={Wellness}
                    options={{ headerShown: false }}
                />
                <Stack.Screen
                    name="Explore"
                    component={Explore}
                    options={{ headerShown: false }}
                />
                <Stack.Screen
                    name="Challenges"
                    component={Challenges}
                    options={{ headerShown: false }}
                />
                <Stack.Screen
                    name="Profile"
                    component={Profile}
                    options={{ headerShown: false }}
                />
                <Stack.Screen
                    name="Progress"
                    component={Progress}
                    options={{ headerShown: false }}
                />
                <Stack.Screen
                    name="WeightReminder"
                    component={WeightReminder}
                    options={{ headerShown: false }}
                />
                <Stack.Screen
                    name="SleepReminder"
                    component={SleepReminder}
                    options={{ headerShown: false }}
                />
                <Stack.Screen
                    name="HomeProfile"
                    component={HomeProfile}
                    options={{ headerShown: false }}
                />
                <Stack.Screen
                    name="Nutrition"
                    component={Nutrition}
                    options={{ headerShown: false }}
                />
                <Stack.Screen
                    name="JournalScreen"
                    component={JournalScreen}
                    options={{ headerShown: false }}
                />
                <Stack.Screen
                    name="RecipeScreen"
                    component={RecipeScreen}
                    options={{ headerShown: false }}
                />
                <Stack.Screen
                    name="StepScreen"
                    component={StepScreen}
                    options={{ headerShown: false }}
                />
                <Stack.Screen
                    name="NutritionDetailScreen"
                    component={NutritionDetailScreen}
                    options={{ headerShown: false }}
                />
                <Stack.Screen
                    name="StepInsightScreen"
                    component={StepInsightScreen}
                    options={{ headerShown: false }}
                />
                <Stack.Screen
                    name="WaterScreen"
                    component={WaterScreen}
                    options={{ headerShown: false }}
                />
                <Stack.Screen
                    name="ShareScreen"
                    component={ShareScreen}
                    options={{ headerShown: false }}
                />
                <Stack.Screen
                    name="CommunityScreen"
                    component={CommunityScreen}
                    options={{ headerShown: false }}
                />
                <Stack.Screen
                    name="PollsDetailScreen"
                    component={PollsDetailScreen}
                    options={{ headerShown: false }}
                />
                <Stack.Screen
                    name="CreatePostScreen"
                    component={CreatePostScreen}
                    options={{ headerShown: false }}
                />
                <Stack.Screen
                    name="InviteScreen"
                    component={InviteScreen}
                    options={{ headerShown: false }}
                />
                <Stack.Screen
                    name="NotificationScreen"
                    component={NotificationScreen}
                    options={{ headerShown: false }}
                />
                <Stack.Screen
                    name="FeedBackScreenOne"
                    component={FeedBackScreenOne}
                    options={{ headerShown: false }}
                />
                <Stack.Screen
                    name="FeedBackScreenTwo"
                    component={FeedBackScreenTwo}
                    options={{ headerShown: false }}
                />

            </Stack.Navigator>

    );
};
const HomeStackNavigator = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen name="Contact" component={HomeScreen} />
        </Stack.Navigator>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#fff',
    },
    scroll: {
        flexGrow: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    inputView: {
        width: '80%',
        backgroundColor: '#fff',
        height: 50,
        marginBottom: 20,
        justifyContent: 'center',
        alignSelf: 'center',
        textAlign: 'left',
        padding: 20,
        borderBottomColor: 'black',
        borderBottomWidth: 1,
        marginTop: -20,
    },
    inputText: {
        height: 50,
        color: 'grey',
    },
    loginBtn: {
        width: '70%',
        backgroundColor: '#F26722',
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        marginTop: 40,
        marginBottom: 10,
        borderWidth: 1,
        borderColor: '#F26522',
    },
    loginText: {
        color: 'white',
    },
    forgotText: {
        color: '#337ab7',
        fontSize: 18,
        fontWeight: 'bold',
        textAlign: 'center',
        marginTop:20,
    },
    signupText: {
        color: '#337ab7',
        fontSize: 18,
        fontWeight: 'bold',
        textAlign: 'center',
    },
    forgotBtn: {
        height: 50,
        width:'100%',
        marginTop:10,
    },
    signupBtn: {
        height: 50,
        width:'100%',
        marginTop:20,
    },
    label: {
        textAlign: 'center',
    },
});

export { MainStackNavigator, HomeStackNavigator };