import React  from 'react';
import {
  SafeAreaView,
  View,
  FlatList,
  StyleSheet,
  Text,
  Image,
  ImageBackground,
  ScrollView, TouchableOpacity,
} from "react-native";


export default class MobileNumber extends React.Component {

  constructor(props) {
    super(props);
    this._onPressButton = this._onPressButton.bind(this);
  }

  _onPressButton(){

    this.props.navigation.navigate('VerifyMobile');
  }

  render() {
    return (

      <SafeAreaView style={styles.container}>

            <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:40}}>
              <View>
                <Image source={require('../images/down.png')} style={{width:24,height:24,marginLeft:20}}></Image>
              </View>
              <View style={{width:119,height:8,alignItems: 'center',flexDirection:'row'}}>
                <View style={{width:59,height:8,borderRadius:5,borderColor:'#7265E3',backgroundColor:'#7265E3',marginTop:15}}></View>
                <View style={{width:59,height:8,borderRadius:5,backgroundColor:'#E1DDF5',marginTop:15}}></View>

              </View>
              <View style={{flexDirection:'row'}}>
                <Text style={{color:'#7265E3',marginRight:20,fontWeight:'bold'}}>Next</Text>
              </View>

          </View>
        <View style={{alignItems :'center',marginTop:30}}>
        <Text style={{color:'#7265E3',fontSize:12,fontWeight: 'bold'}}>STEP 1/2</Text>
        </View>
        <View style={{flexDirection:'column',alignItems: 'center',marginTop:20}}>
        <Text style={{color:'#2D3142',fontSize:24}}>Let's start with your</Text>
          <Text style={{color:'#2D3142',fontSize:24}}>mobile number</Text>
        </View>
        <View style={{alignItems: 'center',marginTop:20}}>
          <Text style={{color:'#4C5980',fontSize:16}}>Number we can use to reach you</Text>
        </View>
        <View style={{alignItems:'center'}}>
        <View style={{backgroundColor:'#FFFFFF',borderRadius:8,width:283,height:60,alignItems: 'center',marginTop:20}}>

          <Text style={{marginTop:20}}>9898989898</Text>
        </View>
          <TouchableOpacity onPress={this._onPressButton} style={{
            marginTop: 70,
            flexDirection: 'row',
            justifyContent: 'center',
            backgroundColor: '#7265E3',
            paddingTop: 15,
            borderRadius: 14,
            width:250,
            height:56
          }}>
            <Text style={{color:'#ffffff',fontWeight:'bold'}}>Verify Now</Text>
          </TouchableOpacity>

        </View>

      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({

  item: {
    backgroundColor: '#F2F2F2',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },
  imageWrapper: {
    height: 200,
    width: 200,
    overflow : "hidden"
  },
  theImage: {
    width: "100%",
    height: "100%",
    resizeMode: "cover",
  },
  container: {
    flex: 1,
    backgroundColor: '#F2F2F2',
    //alignItems: 'stretch',
    //justifyContent: 'center',
    //backgroundColor: '#ecf0f1',
  },
  image: {
    flexGrow:1,
    height:250,
    width:'100%',
    alignItems: 'center',
    justifyContent:'center',
  },
  paragraph: {
    margin: 24,
    fontSize: 48,
    fontWeight: 'bold',
    textAlign: 'center',
    color: 'white',
    backgroundColor: 'transparent',
  },
});
