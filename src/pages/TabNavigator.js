import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

import { MainStackNavigator,HomeStackNavigator } from "./StackNavigator";
import { Ionicons } from '@expo/vector-icons';
import HomeScreen from "./HomeScreen";
import CommunityScreen from "./CommunityScreen";
import Profile from "./Profile";
import SplashScreen from "./SplashScreen";

const Tab = createBottomTabNavigator();
const TabIcon = (props) => (
    <Ionicons
        name={'md-home'}
        size={20}
        color={props.focused ? '#F94702' : '#000000'}
    />
)
const SearchIcon = (props) => (
    <Ionicons
        name={'md-search'}
        size={20}
        color={props.focused ?'#F94702' : '#000000'}
    />
)
const DownloadIcon = (props) => (
    <Ionicons
        name={'md-add-circle-outline'}
        size={20}
        color={props.focused ?'#F94702' : '#000000'}
    />
)
const ShoppingIcon = (props) => (
    <Ionicons
        name={'md-card-outline'}
        size={20}
        color={props.focused ?'#F94702' : '#000000'}
    />
)
const ProfileIcon = (props) => (
    <Ionicons
        name={'md-person-outline'}
        size={20}
        color={props.focused ?'#F94702' : '#000000'}
    />
)


const BottomTabNavigator = () => {
    return (
        <Tab.Navigator
            initialRouteName="SplashScreen"
            activeColor="#e91e63"
            labelStyle={{ fontSize: 12 }}
            style={{ backgroundColor: 'tomato' }}
        >
            <Tab.Screen
                name="HomeScreen"
                component={MainStackNavigator}
                options={{
                    tabBarLabel: 'Home',
                    tabBarIcon: TabIcon,

                }}
            />
            <Tab.Screen
                name="CommunityScreen"
                component={HomeStackNavigator}
                options={{
                    tabBarLabel: 'Updates',
                    tabBarIcon: SearchIcon,
                }}
            />
            <Tab.Screen
                name="Profile"
                component={MainStackNavigator}
                options={{
                    tabBarLabel: 'Profile',
                    tabBarIcon: DownloadIcon,
                }}
            />
        </Tab.Navigator>
    );
};

export default BottomTabNavigator;


