import React  from 'react';
import {
  SafeAreaView,
  View,
  FlatList,
  StyleSheet,
  Text,
  Image,
  ImageBackground,
  ScrollView, TouchableOpacity,TextInput
} from "react-native";


export default class LevelScreen extends React.Component {

  constructor(props) {
    super(props);
    this._onPressButton = this._onPressButton.bind(this);
  }

  _onPressButton(){

    this.props.navigation.navigate('Notification');
  }

  render() {
    return (

      <SafeAreaView style={styles.container}>

        <ScrollView>
          <View style={{flexDirection:'row',marginTop:40}}>
            <View>
              <Image source={require('../images/down.png')} style={{width:24,height:24,marginLeft:20}}></Image>
            </View>
            <View style={{width:119,height:8,alignItems: 'center',flexDirection:'row',marginLeft:80}}>
              <View style={{width:59,height:8,borderRadius:5,borderColor:'#7265E3',backgroundColor:'#7265E3',marginTop:15}}></View>
              <View style={{width:59,height:8,borderRadius:5,backgroundColor:'#E1DDF5',marginTop:15}}></View>

            </View>

          </View>

          <View style={{alignItems :'center',marginTop:30}}>
            <Text style={{color:'#7265E3',fontSize:12,fontWeight: 'bold'}}>STEP 1/2</Text>
          </View>
          <View style={{alignItems:'center',marginTop:20,flexDirection:'column'}}>
            <Text style={{color:'#2D3142',fontSize:24,fontWeight:'bold'}}>What's your current</Text>

            <Text style={{color:'#2D3142',fontSize:24,fontWeight:'bold'}}>fitness level</Text>

          </View>

          <TouchableOpacity onPress={this._onPressButton} >
          <View style={{alignItems:'center',marginTop:20}}>
            <Image source={require('../images/Card1.png')}></Image>
          </View>
          </TouchableOpacity>

        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({

  item: {
    backgroundColor: '#F2F2F2',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },
  imageWrapper: {
    height: 200,
    width: 200,
    overflow : "hidden"
  },
  theImage: {
    width: "100%",
    height: "100%",
    resizeMode: "cover",
  },
  container: {
    flex: 1,
    backgroundColor: '#F2F2F2',
    //alignItems: 'stretch',
    //justifyContent: 'center',
    //backgroundColor: '#ecf0f1',
  },
  image: {
    flexGrow:1,
    height:250,
    width:'100%',
    alignItems: 'center',
    justifyContent:'center',
  },
  paragraph: {
    margin: 24,
    fontSize: 48,
    fontWeight: 'bold',
    textAlign: 'center',
    color: 'white',
    backgroundColor: 'transparent',
  },
});
