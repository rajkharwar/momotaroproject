import React  from 'react';
import {
    SafeAreaView,
    View,
    FlatList,
    StyleSheet,
    Text,
    Image,
    ImageBackground,
    ScrollView, TouchableOpacity, TextInput, CheckBox
} from "react-native";


export default class StepScreen extends React.Component {

    constructor(props) {
        super(props);
        this._onPressButton = this._onPressButton.bind(this);
    }

    _onPressButton(){

        this.props.navigation.navigate('StepInsightScreen');
    }

    render() {
        return (

            <SafeAreaView style={styles.container}>

                <ScrollView>

                    <View style={{
                        flexDirection: 'row',
                        marginLeft: 15,
                        marginTop: 50,
                        justifyContent: 'space-between'
                    }}>
                        <View style={{flexDirection: 'column', marginLeft: 5}}>
                            <TouchableOpacity  onPress={() => this._onPressBackButton()}>
                                <Image  source={require('../images/down.png')} />
                            </TouchableOpacity>

                        </View>
                        <View  style={{marginRight: 10,}}>
                            <TouchableOpacity onPress={this._onPressButton} style={{

                                flexDirection: 'row',
                                justifyContent: 'center',
                                backgroundColor: '#E4DFFF',
                                paddingTop: 15,
                                borderRadius: 14,
                                width:120,
                                height:56,
                            }}>
                                <Text style={{color:'#7265E3',fontWeight:'bold'}}>Insight</Text>
                            </TouchableOpacity>

                        </View>
                    </View>
                    <View style={{alignItems: 'center',marginTop:20}}>
                        <Text style={{color:'#7265E3',fontSize:15}}>DAILY STEPS</Text>
                    </View>
                    <View style={{alignItems: 'center',justifyContent:'center',alignRight:'auto'}}>
                        <Text style={{color: '#2D3142',fontSize:20,fontWeight: 'bold'}}>
                             You have walked{'\n '}
                            <Text style={{color: '#7265E3',fontSize:20,fontWeight: 'bold'}}>
                                40% {''}
                            </Text>
                            <Text style={{color: '#2D3142',fontSize:20,fontWeight: 'bold'}}>
                            of your goal
                            </Text>
                        </Text>
                    </View>
                    <TouchableOpacity onPress={this._onPressButton} >
                        <View style={{alignItems:'center',marginTop:20}}>
                            <Image source={require('../images/MainGraph.png')}></Image>
                        </View>
                    </TouchableOpacity>
                    <View style={{alignItems:'center',marginTop:20}}>
                        <Image source={require('../images/DetailsStep.png')}></Image>
                    </View>
                    <View style={{alignItems:'center',marginTop:20}}>
                        <Image source={require('../images/WeeklyOverview.png')}></Image>
                    </View>


                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: '#F2F2F2',
    },
    image: {
        flexGrow:1,
        height:250,
        width:'100%',
        alignItems: 'center',
        justifyContent:'center',
    },
    nutList:{
        marginLeft:20,
        marginRight:20,
        marginTop:25,
    }

});
