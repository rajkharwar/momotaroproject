import React  from 'react';
import {
    SafeAreaView,
    View,
    FlatList,
    StyleSheet,
    Text,
    Image,
    ImageBackground,
    ScrollView, TouchableOpacity, TextInput, CheckBox
} from "react-native";


export default class WaterScreen extends React.Component {

    constructor(props) {
        super(props);
        this._onPressButton = this._onPressButton.bind(this);
    }

    _onPressButton(){

        this.props.navigation.navigate('ShareScreen');
    }

    render() {
        return (

            <SafeAreaView style={styles.container}>

                <ScrollView>
                    <View style={{
                        flexDirection: 'row',
                        marginLeft: 20,
                        marginTop: 50,}}>
                        <TouchableOpacity  onPress={() => this._onPressBackButton()}>
                            <Image  source={require('../images/down.png')} />
                        </TouchableOpacity>
                    </View>
                    <View style={{alignItems: 'center',marginTop:20}}>
                        <Text style={{color:'#7265E3',fontSize:15}}>HYDRATION</Text>
                    </View>
                    <View style={{alignItems: 'center',justifyContent:'center',alignRight:'auto'}}>
                        <Text style={{color: '#2D3142',fontSize:20,fontWeight: 'bold'}}>
                            Today you took{'\n '}
                            <Text style={{color: '#7265E3',fontSize:20,fontWeight: 'bold'}}>
                                750ml {''}
                            </Text>
                            <Text style={{color: '#2D3142',fontSize:20,fontWeight: 'bold'}}>
                                of water
                            </Text>
                        </Text>
                        <Text style={{color:'#4C5980',fontSize:15}}>Almost there keep hydrated</Text>
                    </View>

                        <View style={{alignItems:'center',marginTop:40}}>
                            <Image source={require('../images/WaterMeasurement.png')}></Image>
                        </View>

                    <View style={{alignItems:'center',marginTop:40}}>
                        <Image source={require('../images/AddDrink.png')}></Image>
                    </View>
                    <View style={{alignItems:'center'}}>
                        <TouchableOpacity onPress={this._onPressButton} style={{
                            marginTop: 40,
                            flexDirection: 'row',
                            justifyContent: 'center',
                            backgroundColor: '#7265E3',
                            paddingTop: 15,
                            borderRadius: 14,
                            width:250,
                            height:56
                        }}>
                            <Text style={{color:'#ffffff',fontWeight:'bold'}}>Add Drink</Text>
                        </TouchableOpacity>
                    </View>

                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: '#F2F2F2',
    },
    image: {
        flexGrow:1,
        height:250,
        width:'100%',
        alignItems: 'center',
        justifyContent:'center',
    },
    nutList:{
        marginLeft:20,
        marginRight:20,
        marginTop:25,
    }

});
