import React  from 'react';
import {
    SafeAreaView,
    View,
    FlatList,
    StyleSheet,
    Text,
    Image,
    ImageBackground,
    ScrollView,TouchableOpacity,
} from 'react-native';
import {createAppContainer,createSwitchNavigator} from "react-navigation";
import { createStackNavigator } from 'react-navigation-stack';
import SplashScreen from "./SplashScreen";
import MobileNumber from "./MobileNumber";
import VerifyMobile from "./VerifyMobile";
import SetPassword from "./SetPassword";
import FingerPrint from "./FingerPrint";
import ProfilePic from "./ProfilePic";
import Interest from "./Interest";
import GoalScreen from "./GoalScreen";
import Gender from "./Gender";
import LevelScreen from "./LevelScreen";
import Notification from "./Notification";
import WeightScreen from "./WeightScreen";
import HomeScreen from "./HomeScreen";
import Wellness from "./Wellness";
import Explore from "./Explore";
import Challenges from "./Challenges";
import Profile from "./Profile";
import Progress from "./Progress"
import WeightReminder from "./WeightReminder";
import SleepReminder from "./SleepReminder";
import HomeProfile from "./HomeProfile";
import Nutrition from "./Nutrition";
import JournalScreen from "./JournalScreen";
import RecipeScreen from "./RecipeScreen";
import StepScreen from "./StepScreen";
import NutritionDetailScreen from "./NutritionDetailScreen";
import StepInsightScreen from "./StepInsightScreen";
import WaterScreen from "./WaterScreen";
import ShareScreen from "./ShareScreen";
import CommunityScreen from "./CommunityScreen";
import PollsDetailScreen from "./PollsDetailScreen";
import CreatePostScreen from "./CreatePostScreen";
import InviteScreen from "./InviteScreen";
import NotificationScreen from "./NotificationScreen";
import FeedBackScreenOne from "./FeedBackScreenOne";
import FeedBackScreenTwo from "./FeedBackScreenTwo";

const AppNavigator = createStackNavigator(

    {
        SplashScreen: {
            screen: SplashScreen,
            navigationOptions: {
                header: null, //No header in this screen
            },
        },
        MobileNumber:{
            screen: MobileNumber,
            navigationOptions: {
                header: null, //No header in this screen
            },
        },
      VerifyMobile:{
            screen: VerifyMobile,
            navigationOptions: {
                header: null, //No header in this screen
            },
        },
      SetPassword:{
            screen: SetPassword,
            navigationOptions: {
                header: null, //No header in this screen
            },
        },
      FingerPrint:{
            screen: FingerPrint,
            navigationOptions: {
                header: null, //No header in this screen
            },
        },
      ProfilePic:{
            screen: ProfilePic,
            navigationOptions: {
                header: null, //No header in this screen
            },
        },

      Interest:{
            screen: Interest,
            navigationOptions: {
                header: null, //No header in this screen
            },
        },
      GoalScreen:{
            screen: GoalScreen,
            navigationOptions: {
                header: null, //No header in this screen
            },
        },
      Gender:{
            screen: Gender,
            navigationOptions: {
                header: null, //No header in this screen
            },
        },
      LevelScreen:{
            screen: LevelScreen,
            navigationOptions: {
                header: null, //No header in this screen
            },
        },
      Notification:{
            screen: Notification,
            navigationOptions: {
                header: null, //No header in this screen
            },
        },
      WeightScreen:{
            screen: WeightScreen,
            navigationOptions: {
                header: null, //No header in this screen
            },
        },
      HomeScreen:{
        screen: HomeScreen,
        navigationOptions: {
          header: null, //No header in this screen
        },
      },
      Wellness:{
        screen: Wellness,
        navigationOptions: {
          header: null, //No header in this screen
        },
      },
      Explore:{
        screen: Explore,
        navigationOptions: {
          header: null, //No header in this screen
        },
      },
        Challenges: {
            screen: Challenges,
            navigationOptions: {
                header: null, //No header in this screen
            },
        },
        Profile: {
            screen: Profile,
            navigationOptions: {
                header: null, //No header in this screen
            },
        },
        Progress: {
            screen: Progress,
            navigationOptions: {
                header: null, //No header in this screen
            },
        },
        WeightReminder: {
            screen: WeightReminder,
            navigationOptions: {
                header: null, //No header in this screen
            },
        },
        SleepReminder: {
            screen: SleepReminder,
            navigationOptions: {
                header: null, //No header in this screen
            },
        },
        HomeProfile: {
            screen: HomeProfile,
            navigationOptions: {
                header: null, //No header in this screen
            },
        },
        Nutrition: {
            screen: Nutrition,
            navigationOptions: {
                header: null, //No header in this screen
            },
        },
        JournalScreen: {
            screen: JournalScreen,
            navigationOptions: {
                header: null, //No header in this screen
            },
        },
        RecipeScreen: {
            screen: RecipeScreen,
            navigationOptions: {
                header: null, //No header in this screen
            },
        },
        StepScreen: {
            screen: StepScreen,
            navigationOptions: {
                header: null, //No header in this screen
            },
        },
        NutritionDetailScreen: {
            screen: NutritionDetailScreen,
            navigationOptions: {
                header: null, //No header in this screen
            },
        },
        StepInsightScreen: {
            screen: StepInsightScreen,
            navigationOptions: {
                header: null, //No header in this screen
            },
        },
        WaterScreen: {
            screen: WaterScreen,
            navigationOptions: {
                header: null, //No header in this screen
            },
        },
        ShareScreen: {
            screen: ShareScreen,
            navigationOptions: {
                header: null, //No header in this screen
            },
        },
        CommunityScreen: {
            screen: CommunityScreen,
            navigationOptions: {
                header: null, //No header in this screen
            },
        },
        PollsDetailScreen: {
            screen: PollsDetailScreen,
            navigationOptions: {
                header: null, //No header in this screen
            },
        },
        CreatePostScreen: {
            screen: CreatePostScreen,
            navigationOptions: {
                header: null, //No header in this screen
            },
        },
        InviteScreen: {
            screen: InviteScreen,
            navigationOptions: {
                header: null, //No header in this screen
            },
        },
        NotificationScreen: {
            screen: NotificationScreen,
            navigationOptions: {
                header: null, //No header in this screen
            },
        },
        FeedBackScreenOne: {
            screen: FeedBackScreenOne,
            navigationOptions: {
                header: null, //No header in this screen
            },
        },
        FeedBackScreenTwo: {
            screen: FeedBackScreenTwo,
            navigationOptions: {
                header: null, //No header in this screen
            },
        },


    },
    {
        initialRouteName:'SplashScreen'
    }

);
const AppContainer = createAppContainer(AppNavigator)
export default AppContainer;
