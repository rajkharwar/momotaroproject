import React  from 'react';
import {
    SafeAreaView,
    View,
    FlatList,
    StyleSheet,
    Text,
    Image,
    ImageBackground,
    ScrollView,
    TouchableOpacity
} from 'react-native';
import Constants from 'expo-constants';
import AppIntroSlider from "react-native-app-intro-slider";
//import Search from "react-native-search-box";
//import SearchList from "react-native-search-list";
//import {TextInput} from "react-native-web";



const rowHeight = 45;
const DATA = [
    {
        id:1,
        image: require('../images/Metrics.png'),

    },
    {
        id:2,
        image: require('../images/Metrics.png'),

    },
    {
        id:3,
        image: require('../images/Metrics.png'),

    },
    {
        id:4,
        image: require('../images/Metrics.png'),

    },
];
const ListItems = [
    {
        id:1,
        image: require('../images/watermelon.png'),
        name: 'Nutrition Expert',
        desc: 'hit all daily goals',

    },
    {
        id:3,
        image: require('../images/watermelon.png'),
        name: 'Nutrition Expert',
        desc: 'hit all daily goals',

    },
    {
        id:3,
        image: require('../images/watermelon.png'),
        name: 'Nutrition Expert',
        desc: 'hit all daily goals',

    },
    {
        id:4,
        image: require('../images/Metrics.png'),

    },
];


function Item({ title }) {
    return (
        <View style={styles.item}>
            <Text style={styles.title}>{title}</Text>
        </View>
    );
}


export default class Profile extends React.Component {

    constructor(props) {
        super(props);
        this._onPressButton = this._onPressButton.bind(this);
    }

    _onPressButton(){

        this.props.navigation.navigate('Progress');
    }



    render() {
        return (

            <SafeAreaView style={styles.container}>
                <ScrollView>

                    <View style={{backgroundColor:'blue' ,height:180}}>

                    </View>
                    <View style={{flexGrow: 1,}}>
                        <Image
                            source={require('../images/avatar.png')}
                            style={{
                                position: 'absolute',
                                top: -40,
                                marginLeft:10,
                                height: 80,
                                width: 80}}
                        />
                    </View>
                    <View style={{
                        flexDirection: 'row',
                        marginLeft: 15,
                        marginTop: 50,
                        justifyContent: 'space-between'
                    }}>
                        <View style={{flexDirection: 'column', marginLeft: 5}}>
                            <Text style={{fontWeight: 'bold', color: "#000000"}}>Grace Saraswati</Text>
                            <Text style={{ color: "#7265E3"}}>Basic Member</Text>

                        </View>
                        <View  style={{marginRight: 10,}}>
                            <TouchableOpacity onPress={this._onPressButton} style={{

                                flexDirection: 'row',
                                justifyContent: 'center',
                                backgroundColor: '#E4DFFF',
                                paddingTop: 15,
                                borderRadius: 14,
                                width:120,
                                height:56,
                            }}>
                                <Text style={{color:'#7265E3',fontWeight:'bold'}}>Edit</Text>
                            </TouchableOpacity>

                        </View>
                    </View>
                    <View style={{marginLeft: 15,marginTop:20}}>
                        <Text style={{ color: "#4C5980"}}>I decided I was going to actively pursue a better life, and take better care of my mind, body and soul</Text>
                    </View>
                    <View style={{flexDirection: 'row',
                        justifyContent:'space-between', marginLeft:15,marginRight:15,marginTop:20}}>
                        <View style={{flexDirection: 'column'}}>
                        <Text style={{color:'#7265E3',fontWeight:'bold'}}>Weight</Text>
                            <Text style={{color:'#2D3142',fontWeight:'bold'}}>55 kg</Text>
                        </View>
                        <View style={{flexDirection: 'column'}}>
                            <Text style={{color:'#7265E3',fontWeight:'bold'}}>Age</Text>
                            <Text style={{color:'#2D3142',fontWeight:'bold'}}>26 yo</Text>
                        </View>
                        <View style={{flexDirection: 'column'}}>
                            <Text style={{color:'#7265E3',fontWeight:'bold'}}>Height</Text>
                            <Text style={{color:'#2D3142',fontWeight:'bold'}}>160 cm</Text>
                        </View>
                    </View>

                    <View>

                        <FlatList


                            numColumns={4}
                            data={DATA}
                            keyExtriactor={(time, index) => index.toString()}
                            renderItem={({item}) => {
                                return (
                                    <View style={{width: 150, height: 312, margin:10}}>
                                        <View style={{width: 150, height: 242, marginLeft: 10}}>
                                            <ImageBackground source={item.image} style={{
                                                width: '100%',
                                                height: '100%',
                                                borderWidth: 1,
                                                borderRadius: 8,
                                                overflow: 'hidden'
                                            }}>
                                            </ImageBackground>
                                        </View>

                                    </View>

                                );
                            }}
                        />

                    </View>
                    <View style={{backgroundColor:'#F2F2F2',height:50}}>

                        <View style={{marginLeft:20,marginRight:20,flexDirection:'row',justifyContent:'space-between'}}>
                            <Text style={{fontSize:16,color:'#4C5980',fontWeight: 'bold'}}>My Badges </Text>
                            <Image source={require('../images/more.png')}></Image>

                        </View>
                    </View>
                    <View style={{backgroundColor:'#ffffff',height:150, marginLeft:20,marginRight:20,borderRadius:8}}>
                        <FlatList
                            renderItem={({item,index}) =>
                                <View style={{justifyContent:'center'}}>


                                    {
                                        ListItems.map(user => {

                                                const {id,name,desc,image} = user;
                                                const d = {id,name,desc,image};

                                                return (

                                                    <View style={{flexDirection:'row',alignItems:'center'}}>
                                                        <View style={{height: 70,flexDirection:'row'}}>
                                                            <TouchableOpacity>
                                                                <Image source={require('../images/Metrics.png')} style={{height:50,width:50,marginTop:10,marginLeft:20}}/>
                                                            </TouchableOpacity>

                                                            <Text style={styles.item}>
                                                                {d.name}
                                                            </Text>
                                                        </View>
                                                    </View>
                                                );

                                            }
                                        )
                                    }

                                </View>
                            }
                            keyExtractor={(item,index) => item.index}
                            ItemSeparatorComponent={this.renderSeparator}
                        />
                        </View>
                    <View style={{backgroundColor:'#ffffff',height:250, marginLeft:20,marginRight:20,borderRadius:8, marginTop:20}}>
                        <Text style={{ marginLeft:20,marginTop: 15, color: "#7265E3"}}>Go Premimum</Text>
                        <View>
                            <Text style={{ color: "#2D3142", marginLeft:20,fontWeight: 'bold',marginTop:20}}>Unlock all features to improve your health</Text>

                        </View>
                        <View style={{flexDirection:'row',marginLeft: 20,marginTop: 20}}>
                            <Image source={require('../images/avatar.png')} style={{height:50,width:50,marginLeft:8}}/>
                            <Image source={require('../images/avatar.png')} style={{height:50,width:50,marginLeft:8}}/>
                            <Image source={require('../images/avatar.png')} style={{height:50,width:50,marginLeft:8}}/>
                            <Image source={require('../images/avatar.png')} style={{height:50,width:50,marginLeft:8}}/>
                        </View>
                        <View style={{alignItems:'center'}}>
                            <TouchableOpacity onPress={this._onPressButton} style={{
                                marginTop: 20,
                                flexDirection: 'row',
                                justifyContent: 'center',
                                backgroundColor: '#7265E3',
                                paddingTop: 15,
                                borderRadius: 14,
                                width:250,
                                height:56
                            }}>
                                <Text style={{color:'#ffffff',fontWeight:'bold'}}>Unlock Now</Text>
                            </TouchableOpacity>
                        </View>



                    </View>


                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({

    item: {
        backgroundColor: '#ffffff',
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,

    },
    title: {
        fontSize: 32,
    },
    imageWrapper: {
        height: 200,
        width: 200,
        overflow : "hidden"
    },
    theImage: {
        width: "100%",
        height: "100%",
        resizeMode: "cover",
    },
    container: {
        flex: 1,
        //alignItems: 'stretch',
        //justifyContent: 'center',
        paddingTop: Constants.statusBarHeight,
        //backgroundColor: '#ecf0f1',
    },
    image: {
        //flexGrow:1,
        height:250,
        width:'100%',
        alignItems: 'center',
        justifyContent:'center',
    },
    paragraph: {
        margin: 24,
        fontSize: 48,
        fontWeight: 'bold',
        textAlign: 'center',
        color: 'white',
        backgroundColor: 'transparent',
    },

});
