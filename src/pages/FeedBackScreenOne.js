import React  from 'react';
import {
    SafeAreaView,
    View,
    FlatList,
    StyleSheet,
    Text,
    Image,
    ImageBackground,
    ScrollView, TouchableOpacity, TextInput, CheckBox
} from "react-native";



export default class FeedBackScreenOne extends React.Component {

    constructor(props) {
        super(props);
        this._onPressButton = this._onPressButton.bind(this);
    }

    _onPressButton(){

        this.props.navigation.navigate('FeedBackScreenTwo');
    }

    render() {
        return (

            <SafeAreaView style={styles.container}>

                <ScrollView>

                    <View style={{
                        flexDirection: 'row',
                        marginLeft: 20,
                        marginTop: 70,
                    }}>
                        <TouchableOpacity  onPress={() => this._onPressBackButton()}>
                            <Image  source={require('../images/arrow.png')} />
                        </TouchableOpacity>
                    </View>
                    <View style={{alignItems: 'center',marginTop:20}}>
                        <Text style={{color:'#7FE3F0',fontSize:15}}>USER FEEDBACK</Text>
                    </View>
                    <View style={{alignItems: 'center',justifyContent:'center',alignRight:'auto'}}>
                        <Text style={{color: '#FFFFFF',fontSize:20,fontWeight: 'bold'}}>
                            How was your overall{'\n'}
                            <Text style={{color: '#FFFFFF',fontSize:20,fontWeight: 'bold'}}>
                                experience?
                            </Text>
                        </Text>
                    </View>
                    <TouchableOpacity onPress={this._onPressButton}>
                        <View style={{alignItems:'center',marginTop:50}}>
                            <Image source={require('../images/smiley.png')}></Image>
                        </View>
                    </TouchableOpacity>

                        <View style={{alignItems:'center',marginTop:50}}>
                            <Image source={require('../images/downArrow.png')}></Image>
                        </View>


                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: '#7265E3',
    },
    listItem:{
        margin:10,
        //  padding:10,
        width:"90%",
        flex:1,
        alignSelf:"center",
        flexDirection:"row",
        borderRadius:5
    }
});
