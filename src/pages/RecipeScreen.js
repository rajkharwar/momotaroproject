import React  from 'react';
import {
    SafeAreaView,
    View,
    FlatList,
    StyleSheet,
    Text,
    Image,
    ImageBackground,
    ScrollView,
    TouchableOpacity
} from 'react-native';
import Constants from 'expo-constants';
const DATA = [
    {
        id:1,
        image: require('../images/PlateRecipe.png'),


    },
    {
        id:2,
        image: require('../images/PlateRecipe.png'),

    },
    {
        id:3,
        image: require('../images/PlateRecipe.png'),


    },
    {
        id:4,
        image: require('../images/PlateRecipe.png'),


    },
];

const PROD = [
    {
        id:1,
        image: require('../images/calories.png'),
        title: 'Salad with wheat and white egg',
        desc: '200 cals',

    },
    {
        id:2,
        image: require('../images/calories.png'),
        title: 'Salad with wheat and white egg',
        desc: '200 cals',

    },
    {
        id:3,
        image: require('../images/calories.png'),
        title: 'Salad with wheat and white egg',
        desc: '200 cals',

    },
    {
        id:4,
        image: require('../images/calories.png'),
        title: 'Salad with wheat and white egg',
        desc: '200 cals',

    },
];


export default class RecipeScreen extends React.Component {

    constructor(props) {
        super(props);
        //this.state = {products: DATA};
        this._onPressButton = this._onPressButton.bind(this);
    }

    _onPressButton(){

        this.props.navigation.navigate('StepScreen');
    }

    render() {
        return (

            <SafeAreaView style={styles.container}>
                <ScrollView>

                    <View style={{backgroundColor:'#8C80F8' ,height:580,borderBottomRightRadius:25,borderBottomLeftRadius:25}}>
                        <View style={{flexDirection:'row', marginTop:20,marginHorizontal:10,alignItems:'center'}}>
                            <TouchableOpacity  onPress={() => this._onPressBackButton()}>
                                <Image  source={require('../images/arrow.png')} />
                            </TouchableOpacity>
                        </View>
                        <View style={{justifyContent:'center',alignItems:'center', marginTop:20}}>
                            <Text style={{color:'#FFFFFF',fontSize:15}}>DAILY PICK</Text>
                        </View>
                        <View style={{justifyContent:'center',alignItems:'center'}}>
                            <Text style={{color:'#FFFFFF',fontSize:20,fontWeight:'bold'}}>Breakfast Ideas</Text>
                        </View>

                        <View style={{height: 230, marginTop: 30,}}>
                            <FlatList
                                horizontal={true}
                                showsHorizontalScrollIndicator={false}
                                data={DATA}
                                keyExtriactor={(time, index) => index.toString()}
                                renderItem={({item}) => {
                                    return (
                                        <View>
                                            <View style={{
                                                width: 310,
                                                height: 200,
                                                //marginLeft: 10,
                                               // borderRadius: 8
                                            }}>

                                                <View style={{
                                                    flexDirection: 'column',
                                                    justifyContent: 'center',
                                                    flex: 1,
                                                    alignItems: 'center'
                                                }}>
                                                    <Image source={item.image}>
                                                    </Image>
                                                </View>

                                            </View>
                                        </View>

                                    );
                                }}
                            />

                        </View>
                        <View style={{justifyContent:'center',alignItems:'center', marginTop:20}}>
                            <Text style={{color:'#FFFFFF',fontSize:15}}>Start your day off right with these  </Text>
                        </View>
                        <View style={{justifyContent:'center',alignItems:'center'}}>
                            <Text style={{color:'#FFFFFF',fontSize:15}}>healthy breakfast recipes  </Text>
                        </View>

                        <View style={{flexDirection:'row', marginTop:20,marginHorizontal:10,marginLeft:70}}>
                            <TouchableOpacity  onPress={() => this._onPressBackButtons()}>
                                <Image  source={require('../images/Bookmark.png')} />
                            </TouchableOpacity>
                            <View style={{flex: 1,justifyContent:'center',alignItems:'center'}}>
                                <TouchableOpacity onPress={this._onPressButton} style={{
                                    flexDirection: 'row',
                                    justifyContent: 'center',
                                    backgroundColor: '#FFFFFF',
                                    paddingTop: 15,
                                    borderRadius: 14,
                                    width:230,
                                    height:56
                                }}>
                                    <Text style={{color:'#7265E3',fontWeight:'bold'}}>Let's Try</Text>
                                </TouchableOpacity>
                            </View>
                        </View>

                    </View>
                    <View style={{marginLeft:20,marginRight:20,marginTop:10,flexDirection:'row',justifyContent:'space-between'}}>
                        <Text style={{fontSize:16,color:'#4C5980',fontWeight: 'bold'}}>New Recipe </Text>
                        <Image source={require('../images/more.png')}></Image>

                    </View>
                    <View style={{height: 130, marginTop: 24, marginLeft: 10}}>
                        <FlatList

                            horizontal={true}
                            showsHorizontalScrollIndicator={false}
                            data={PROD}
                            keyExtriactor={(time, index) => index.toString()}
                            renderItem={({item}) => {
                                return (
                                    <View style={styles.listItem}>
                                        <View style={{flex:1,marginLeft:12,marginTop:15}}>
                                            <Text style={{fontWeight:"bold"}}>{item.title}</Text>
                                            <Text>{item.desc}</Text>
                                        </View>
                                        <Image source={item.image}  style={{width:90, height:120,borderRadius:30}} />
                                    </View>


                                );
                            }}
                        />

                    </View>


                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({

    item: {
        backgroundColor: '#ffffff',
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,

    },
    title: {
        fontSize: 32,
    },

    container: {
        flex: 1,
        //alignItems: 'stretch',
        //justifyContent: 'center',
        paddingTop: Constants.statusBarHeight,
        //backgroundColor: '#ecf0f1',
    },
    image: {
        //flexGrow:1,
        height:250,
        width:'100%',
        alignItems: 'center',
        justifyContent:'center',
    },
    listItem:{
        margin:10,
        //  padding:10,
        backgroundColor:"#FFF",
        width:270,
        height: 130,
        flex:1,
       // alignSelf:"center",
        flexDirection:"row",
        borderRadius:10
    }
});
