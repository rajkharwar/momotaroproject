import React  from 'react';
import {
  SafeAreaView,
  View,
  FlatList,
  StyleSheet,
  Text,
  Image,
  ImageBackground,
  ScrollView, TouchableOpacity,TextInput
} from "react-native";


export default class SetPassword extends React.Component {

  constructor(props) {
    super(props);
    this._onPressButton = this._onPressButton.bind(this);
  }

  _onPressButton(){

    this.props.navigation.navigate('FingerPrint');
  }

  render() {
    return (

      <SafeAreaView style={styles.container}>

        <View style={{flexDirection:'row',marginTop:40}}>
          <View>
            <Image source={require('../images/down.png')} style={{width:24,height:24,marginLeft:20}}></Image>
          </View>
          <View style={{width:119,height:8,alignItems: 'center',flexDirection:'row',marginLeft:80}}>
            <View style={{width:59,height:8,borderRadius:5,borderColor:'#7265E3',backgroundColor:'#7265E3',marginTop:15}}></View>
            <View style={{width:59,height:8,borderRadius:5,backgroundColor:'#E1DDF5',marginTop:15}}></View>

          </View>


        </View>
        <View style={{alignItems :'center',marginTop:30}}>
          <Text style={{color:'#7265E3',fontSize:12,fontWeight: 'bold'}}>STEP 1/2</Text>
        </View>
        <View style={{flexDirection:'column',alignItems: 'center',marginTop:20}}>
          <Text style={{color:'#2D3142',fontSize:24,fontWeight: 'bold'}}>Set your password</Text>
        </View>
        <View style={{alignItems:'center'}}>
          <View style={{width:300,height:60,backgroundColor:'#FFFFFF',borderRadius:12,marginTop:30,alignItems:'center'}}>
            <TextInput style={{ marginTop:20,fontSize:16}} secureTextEntry={true} onChangeText={(password) => this.setState({password})}
                       value={''}
                       placeholder=" "
                       placeholderTextColor='#969490'/>
          </View>

          <View style={{width:293,alignItems: 'center',flexDirection:'row',marginLeft:30,marginTop:20}}>
            <View style={{width:59,height:8,borderRadius:5,borderColor:'#7265E3',backgroundColor:'#7265E3',marginTop:15}}></View>
            <View style={{width:59,height:8,borderRadius:5,borderColor:'#7265E3',backgroundColor:'#7265E3',marginTop:15}}></View>
            <View style={{width:59,height:8,borderRadius:5,borderColor:'#7265E3',backgroundColor:'#7265E3',marginTop:15}}></View>

            <View style={{width:59,height:8,borderRadius:5,backgroundColor:'#E1DDF5',marginTop:15}}></View>

          </View>

          <View>

          <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:40}}>

            <Text style={{fontSize:14,color:'#4C5980',fontWeight: 'bold',margin:20}}>8+ characters</Text>

            <Text style={{fontSize:14,color:'#4C5980',fontWeight: 'bold',margin:20}}>1 Uppercase</Text>
          </View>
          </View>

          <View>

            <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:10}}>

              <Text style={{fontSize:14,color:'#4C5980',fontWeight: 'bold',marginRight:70}}>1 Symbols</Text>

              <Text style={{fontSize:14,color:'#4C5980',fontWeight: 'bold',marginLeft:10}}>1 number</Text>
            </View>
          </View>
          <TouchableOpacity onPress={this._onPressButton} style={{
            marginTop: 70,
            flexDirection: 'row',
            justifyContent: 'center',
            backgroundColor: '#7265E3',
            paddingTop: 15,
            borderRadius: 14,
            width:250,
            height:56
          }}>
            <Text style={{color:'#ffffff',fontWeight:'bold'}}>Continue</Text>
          </TouchableOpacity>

        </View>

      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({

  item: {
    backgroundColor: '#F2F2F2',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },
  imageWrapper: {
    height: 200,
    width: 200,
    overflow : "hidden"
  },
  theImage: {
    width: "100%",
    height: "100%",
    resizeMode: "cover",
  },
  container: {
    flex: 1,
    backgroundColor: '#F2F2F2',
    //alignItems: 'stretch',
    //justifyContent: 'center',
    //backgroundColor: '#ecf0f1',
  },
  image: {
    flexGrow:1,
    height:250,
    width:'100%',
    alignItems: 'center',
    justifyContent:'center',
  },
  paragraph: {
    margin: 24,
    fontSize: 48,
    fontWeight: 'bold',
    textAlign: 'center',
    color: 'white',
    backgroundColor: 'transparent',
  },
});
