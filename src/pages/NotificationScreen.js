import React  from 'react';
import {
    SafeAreaView,
    View,
    FlatList,
    StyleSheet,
    Text,
    Image,
    ImageBackground,
    ScrollView, TouchableOpacity, TextInput, CheckBox
} from "react-native";

const DATA = [
    {
        id:1,
        image: require('../images/Bg1.png'),
        title:'Tommy PArker',
        desc:'Commented on your post',


    },
    {
        id:2,
        image: require('../images/Bg1.png'),
        title:'Drink More',
        desc:'You drink less water',


    },
];
const LIST = [
    {
        id:1,
        image: require('../images/Bg1.png'),
        title:'Daily Target',
        desc:'Commented on your post',


    },

];
const ITEM = [
    {
        id:1,
        image: require('../images/Bg1.png'),
        title:'Achievment',
        desc:'Commented on your post',


    },
    {
        id:2,
        image: require('../images/Bg1.png'),
        title:'Nutrition',
        desc:'You drink less water',


    },
];



export default class NotificationScreen extends React.Component {

    constructor(props) {
        super(props);
        this._onPressbutton = this._onPressbutton.bind(this);
    }

    _onPressbutton(){

        this.props.navigation.navigate('FeedBackScreenOne');
    }

    render() {
        return (

            <SafeAreaView style={styles.container}>

                <ScrollView>

                    <View style={{
                        flexDirection: 'row',
                        marginLeft: 20,
                        marginTop: 50,}}>
                        <TouchableOpacity  onPress={() => this._onPressBackButton()}>
                            <Image  source={require('../images/down.png')} />
                        </TouchableOpacity>
                    </View>
                    <View style={{marginTop:20, marginLeft: 20}}>
                        <Text style={{color:'#000000',fontSize:25,fontWeight:'bold'}}>Notification</Text>
                    </View>

                    <View style={{
                        flexDirection: 'row',
                        marginLeft: 15,
                        marginTop: 20,
                    }}>
                        <View style={{flexDirection: 'column', marginLeft: 15}}>
                            <Text style={{fontSize:16,color:'#4C5980',fontWeight: 'bold'}}>Earlier </Text>
                        </View>
                        <View style={{flexDirection: 'column', marginLeft: 15}}>

                            <Image  source={require('../images/Number.png')} />
                        </View>
                    </View>

                    <View>
                        <TouchableOpacity onPress={this._onPressbutton}>
                        <FlatList
                            numColumns={1}
                            data={DATA}
                            keyExtriactor={(time, index) => index.toString()}
                            renderItem={({item}) => {
                                return (
                                    <View style={styles.listItem}>
                                        <Image source={item.image}  style={{width:60, height:60,borderRadius:30}} />
                                        <View style={{marginLeft:20,flex:1}}>
                                            <Text style={{fontWeight:"bold"}}>{item.title}</Text>
                                            <Text>{item.desc}</Text>
                                        </View>
                                    </View>

                                );
                            }}
                        />
                        </TouchableOpacity>
                    </View>
                    <View style={{marginTop:20, marginLeft: 20}}>
                        <Text style={{fontSize:16,color:'#4C5980',fontWeight: 'bold'}}>Last Week</Text>
                    </View>

                    <View>
                        <FlatList
                            numColumns={1}
                            data={LIST}
                            keyExtriactor={(time, index) => index.toString()}
                            renderItem={({item}) => {
                                return (
                                    <View style={styles.listItem}>
                                        <Image source={item.image}  style={{width:60, height:60,borderRadius:30}} />
                                        <View style={{marginLeft:20,flex:1}}>
                                            <Text style={{fontWeight:"bold"}}>{item.title}</Text>
                                            <Text>{item.desc}</Text>
                                        </View>
                                    </View>

                                );
                            }}
                        />
                    </View>
                    <View style={{marginTop:10,alignItems: 'center'}}>
                        <TouchableOpacity style={{
                            flexDirection: 'row',
                            justifyContent: 'center',
                            backgroundColor: '#E4DFFF',
                            paddingTop: 10,
                            borderRadius: 14,
                            width:120,
                            height:46,
                        }}>
                            <Text style={{color:'#7265E3',fontWeight:'bold'}}>Share</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{marginTop:20}}>
                        <FlatList
                            numColumns={1}
                            data={ITEM}
                            keyExtriactor={(time, index) => index.toString()}
                            renderItem={({item}) => {
                                return (
                                    <View style={styles.listItem}>
                                        <Image source={item.image}  style={{width:60, height:60,borderRadius:30}} />
                                        <View style={{marginLeft:20,flex:1}}>
                                            <Text style={{fontWeight:"bold"}}>{item.title}</Text>
                                            <Text>{item.desc}</Text>
                                        </View>
                                    </View>

                                );
                            }}
                        />
                    </View>

                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: '#F2F2F2',
    },
    listItem:{
        margin:10,
        //  padding:10,
        width:"90%",
        flex:1,
        alignSelf:"center",
        flexDirection:"row",
        borderRadius:5
    }
});
